/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto;

import java.io.StringReader;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

public class CustomResolver implements EntityResolver {
  public InputSource resolveEntity (String publicId, String systemId) {

    StringReader strReader = new StringReader("This is a custom entity");

	System.out.println(" sys id = " + systemId);
	System.out.println(" pub id = " + publicId);
	
/*
    if
(systemId.equals("http://www.builder.com/xml/entities/MyCus
tomEntity")) {
       System.out.println("Resolving entity: " + publicId);
       return new InputSource(strReader);
     } else {
       return null;
     }
	 */
	return null;	 
   }
 } 

