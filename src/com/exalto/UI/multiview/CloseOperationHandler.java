/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.multiview;

public interface CloseOperationHandler {

	/**
      * Perform the closeOperation on the opened elements in the multiview topcomponent.
      * Can resolve by itself just based on the states of the elements or ask the user for
      * the decision.
      * @param elements {@link org.netbeans.core.spi.multiview.CloseOperationState} instances of {@link org.netbeans.core.spi.multiview.MultiViewElement}s that cannot be
      * closed and require resolution.
      * @returns true if component can be close, false if it shall remain opened.
      */
     boolean resolveCloseOperation(CloseOperationState[] elements);

}
