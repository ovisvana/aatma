/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.multiview;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class MVTextElem extends MVElem {
	
	 private JTextPane  textPane;
	 
	 public MVTextElem(JTextPane textPane) {
		 
	//	 textPane = new JTextPane();
		 this.textPane = textPane;
		 
	//	 textPane.setText("This is a test");
		 
		 visualRepre = new JPanel();
		 
		 visualRepre.add(textPane);
		 
	 }

	 public MVTextElem() {
		 
				 textPane = new JTextPane();
				 
				 textPane.setText("This is a test");
				 
				 visualRepre = new JPanel();
				 
				 visualRepre.add(textPane);
				 
			 }

	 public MVTextElem(JPanel parent) {
			super();
			
			 textPane = new JTextPane();
			 
			 textPane.setText("This is a test");
			
			 textPane.setBorder(BorderFactory.createEmptyBorder(1,1,1,1));
		//	 visualRepre = new JPanel();
			 
			 visualRepre.setBackground(Color.white);
			
			 textPane.setSize(new Dimension(500, 500));
				
			 
			 visualRepre.add(textPane);			
			
			 visualRepre.setSize(new Dimension(500, 500));
			
			
		}
	 
	 public javax.swing.JComponent  getVisualRepresentation() {
         return textPane;
     }

}
