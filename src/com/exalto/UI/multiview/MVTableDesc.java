/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.multiview;

import java.awt.Image;


public class MVTableDesc extends MVDesc {

	  public MVTableDesc(String  name, Image  img, int persType, MultiViewElement element) {
          el = element;
          this.name = name;
          this.img = img;
          type = persType;
      }
    
	
    public MultiViewElement createElement() {
        if (el == null) {
            // for persistence.. elem is transient..
       	 el = new MVTableElem();
        }
        
        return el;
    }


}
