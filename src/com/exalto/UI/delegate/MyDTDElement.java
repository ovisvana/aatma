/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.delegate;

import java.util.Vector;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author Nupura
 * Created on Sep 24, 2003
 */
public class MyDTDElement {

	private String name;  // name of the element
	private Vector innerElems;  // name of inner elements
	private ArrayList cardinalities;
	private Hashtable attributes;
	private boolean hasAttributes = false;
	
	public MyDTDElement() {
		super();
		name ="";
		innerElems = new Vector();
		cardinalities = new ArrayList();
		attributes =  new Hashtable();
	}
	
	public void setName(String str)
	{
		name = str;
	}


	public String getName()
	{
		return name;
	}

	public Vector getInnerElems()
	{
		return innerElems;
	}
	public void setInnerElems(String element)
	{
		innerElems.add(element);   
	}    
    
	public ArrayList getCardinalities()
	{
		return cardinalities;
	}
	public void addCardinality(int card)
	{
		cardinalities.add(new Integer(card));   
	}    
    
	public Hashtable getAttributes()
	{
		return attributes;
	}
	public void setattributes(Hashtable atts)
	{
		//attributes.put(attribute);
		attributes = atts;
		if(attributes.size() != 0)
			hasAttributes = true;
	}    

	public boolean hasAttributes()
	{
		
		return hasAttributes ;
	}    
}
