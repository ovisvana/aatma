/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.util;

public interface AppenderUI {

	public void doLog(String s);
	public void setDocument(javax.swing.text.Document sd);
	public void setVisible(boolean f);
	public javax.swing.text.Document getDocument();

}
