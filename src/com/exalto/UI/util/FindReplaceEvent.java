/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.util;

import java.util.EventObject;

/**
 * This is simply overriding EventObject by storing a FindReplaceDialog
 * into the source field of the superclass.
 *
 * @author Ulrich Hilger
 * @author CalCom
 * @author <a href="http://www.calcom.de">http://www.calcom.de</a>
 * @author <a href="mailto:info@calcom.de">info@calcom.de</a>
 *
 * @version 1.1, April 13, 2002
 */

public class FindReplaceEvent extends EventObject {

  public FindReplaceEvent(FindReplaceDialog source) {
    super(source);
  }
}
