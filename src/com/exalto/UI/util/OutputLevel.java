/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.util;

import org.apache.log4j.Level;


// Referenced classes of package org.apache.log4j:
//            Priority

public class OutputLevel extends Level
{

    public OutputLevel(int level, String levelStr, int syslogEquivalent)
    {
        super(level, levelStr, syslogEquivalent);
    }

   

    public static OutputLevel toLevel(int val, Level defaultLevel)
    {
         return Output;
    }

    public static OutputLevel toLevel(String sArg, Level defaultLevel)
    {
        
            return Output;
        
       
    }

    public static final OutputLevel Output = new OutputLevel(60000, "OUTPUT",
0);

}
