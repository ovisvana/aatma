/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.grid.xpath;

//$Id: XPathDemo.java,v 1.1 2004/11/24 23:45:45 jsuttor Exp $
//Copyright 2004 Sun Microsystems, Inc. All rights reserved.

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jaxen.Navigator;
import org.jaxen.XPath;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.exalto.UI.grid.ActionsHandler;
import com.exalto.UI.grid.AlternateRowHighlighter;
import com.exalto.UI.grid.ExaltoXmlNode;
import com.exalto.UI.grid.HierarchicalColumnHighlighter;
import com.exalto.UI.grid.Highlighter;
import com.exalto.UI.grid.HighlighterPipeline;
import com.exalto.UI.grid.JXmlTreeTable;
import java.awt.Color;
import java.util.Set;
import javax.xml.XMLConstants;



public class JaxenXPathEvaluator {
	
  /**
   * <p>usage java XPathDemo &lt;XML file&gt; &lt;XPath Expression&gt;</p>
   * 
   * <p>Apply XPath Express against XML file and
   * output resulting NodeList.</p>
   */
	NamespaceContextImpl namespaceContextImpl;
//	XPathFactory xpf;
//	XPath xpath;
	Document xmlDoc;
	ActionsHandler treeModel;
	
	ExaltoXmlNode foundNode;

	private List listeners = new ArrayList();
	
	String xpathExpression;
	
	JXmlTreeTable treeTable;

	public JaxenXPathEvaluator() {
	
	}
	public JaxenXPathEvaluator(Document doc) {
		this.xmlDoc = doc;
	}
	
	public JaxenXPathEvaluator(JXmlTreeTable treeTable, ActionsHandler treeModel, String [][] namespaces) {

		// for jaxp
	//	System.setProperty("javax.xml.xpath.XPathFactory:http://java.sun.com/jaxp/xpath/dom", "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl"); 

	    // Following is specific to Saxon: should be in a properties file
	//	 System.setProperty("javax.xml.xpath.XPathFactory:"+NamespaceConstant.OBJECT_MODEL_SAXON,
	//	                  "net.sf.saxon.xpath.XPathFactoryImpl");

		
		try {
			// create XPath
	//		xpf = XPathFactory.newInstance(NamespaceConstant.OBJECT_MODEL_SAXON);
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	//	xpath = xpf.newXPath();
		
		namespaceContextImpl = new NamespaceContextImpl();

		this.treeModel = treeModel;
		
		this.treeTable = treeTable;
		
		this.xmlDoc = treeModel.getDocument();
		
		addPropertyChangeListener(treeTable);
		
	
	
	}
	
	public void bindPrefixToNamespace(XPath expression) {

        try {

            		//		    expression.addNamespace("f",
		//		     "http://namespaces.cafeconleche.org/xmljava/ch3/");
		//		    expression.addNamespace("SOAP",
		//		     "http://schemas.xmlsoap.org/soap/envelope/");

            HashMap namespaces = treeModel.getNamespaces();
            
            Set set = namespaces.keySet();

             Iterator iterator = set.iterator();
             while(iterator.hasNext()) {
                 String nmsp = (String) iterator.next();
                 String nmspval = (String) namespaces.get(nmsp);


                nmsp = nmsp.substring(nmsp.indexOf("xmlns:")+6);

           		System.out.println(" nmsp = " + nmsp);
           		System.out.println(" nmspval = " + nmspval);

                 expression.addNamespace(nmsp, nmspval);
             }


              expression.addNamespace(
                      XMLConstants.DEFAULT_NS_PREFIX,
                      "http://schemas.xmlsoap.org/wsdl/");

              expression.addNamespace(
                      "tns",
                      "http://hello.org/wsdl");
              expression.addNamespace(
                      "ns2",
                      "http://hello.org/types");
              expression.addNamespace(
                      "xsd",
                      "http://www.w3.org/2001/XMLSchema");
              expression.addNamespace(
                      "soap",
                      "http://schemas.xmlsoap.org/wsdl/soap/");
              expression.addNamespace(
                      "soap11-enc",
                      "http://schemas.xmlsoap.org/soap/encoding/");
              expression.addNamespace(
                      "xsi",
                      "http://www.w3.org/2001/XMLSchema-instance");
              expression.addNamespace(
                      "wsdl",
                      "http://schemas.xmlsoap.org/wsdl/");
                 
              
        } catch(Exception e) {
            e.printStackTrace();
        }

	}
	
	public List evaluate(String xpathExpression, boolean updateView) {
		
		NodeList nodeList = null;
		Iterator iterator = null;
		List results = null;
		
		this.xpathExpression = xpathExpression;
		
			   try {

             //      treeTable.repaint();
             //      treeTable.revalidate();

                
                   if(updateView) {
                        treeTable.setHighlighters(null);
                        Highlighter[]   highlighters = new Highlighter[] {
                            new AlternateRowHighlighter(Color.white,
                                                         new Color(0xF0, 0xF0, 0xE0), null),
                            new HierarchicalColumnHighlighter(Color.WHITE, new Color(0xF0, 0xF0, 0xE0), treeTable.getSelectionBackground())
                        };

                      HighlighterPipeline highlighterPipeline = new HighlighterPipeline(highlighters);
                      treeTable.setHighlighters(highlighterPipeline);
                   }
                   

                    // There are different XPath classes in different packages
				    // for the different APIs Jaxen supports
				    XPath expression = new org.jaxen.dom.DOMXPath(
				     xpathExpression);

                    Navigator navigator = expression.getNavigator();

                    if(updateView)
                          this.bindPrefixToNamespace(expression);

                    Node selectedNode = null;
                    if(updateView) 
                        selectedNode = this.treeModel.getSelectedNode();

		//		    results = expression.selectNodes(selectedNode);
           		    results = expression.selectNodes(xmlDoc);
				    iterator = (Iterator) results.iterator();

                    //   while (iterator.hasNext()) {
				 //     Node result = (Node) iterator.next();
				 //     String value = StringFunction.evaluate(result, navigator);
				 //     System.out.println(value);
				 //   }
				  
			 
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				

		if(updateView)
			processNodes(iterator);
		
		return results;

	}

	
	
	public void processNodes(Iterator iterator) {

		// dump each Node's info
		Node node = null;
		while(iterator.hasNext()) {
			
			node = (Node) iterator.next();
		
			dumpNode(node);
			
			updateTable(node);

		}
			
	}
	
	
	public void updateTable(Node node) {
		
		HashMap domToTreeMap = treeModel.getDomToTreeMap();

		ExaltoXmlNode oldValue = foundNode; 
		
		foundNode = (ExaltoXmlNode) domToTreeMap.get(node);
		
		 for(int i=0; i<listeners.size();i++) {
			 PropertyChangeListener listener = (PropertyChangeListener)listeners.get(i);
			 
			 PropertyChangeObject oldProp = new PropertyChangeObject();
			 
			 oldProp.setFoundNode(oldValue);
			 oldProp.setXpathExpression(null);
			 
			 PropertyChangeObject newProp = new PropertyChangeObject();
			 newProp.setFoundNode(foundNode);
			 newProp.setXpathExpression(xpathExpression);
			 
			 listener.propertyChange(new PropertyChangeEvent(this, "foundNode", oldProp, newProp));
		 }

		
		
	}
	
	
	
	static void dumpNode(String objectModel,
			String inputFile,
			String xpathExpression,
			NodeList nodeList) {
		
		System.out.println("Object model: " + objectModel + "created from: " + inputFile + "\n"
				+ "XPath expression: " + xpathExpression + "\n"
				+ "NodeList.getLength(): " + nodeList.getLength());
		
		// dump each Node's info
		for (int onNode = 0; onNode < nodeList.getLength(); onNode++) {
			
			Node node = nodeList.item(onNode);
			String nodeName = node.getNodeName();
			String nodeValue = node.getNodeValue();
			if (nodeValue == null) {
				nodeValue = "null";
			}
			String namespaceURI = node.getNamespaceURI();
			if (namespaceURI == null) {
				namespaceURI = "null";
			}
			String namespacePrefix = node.getPrefix();
			if (namespacePrefix == null) {
				namespacePrefix = "null";
			}
			String localName = node.getLocalName();
			if (localName == null) {
				localName = "null";
			}
			
			System.out.println("result #: " + onNode + "\n"
					+ "\tNode name: " + nodeName + "\n"
					+ "\tNode value: " + nodeValue + "\n"
					+ "\tNamespace URI: " + namespaceURI + "\n"
					+ "\tNamespace prefix: " + namespacePrefix + "\n"
					+ "\tLocal name: " + localName);
		}
		// dump each Node's info
		
	}
	
	
	
	static void dumpNode(Node node) {
		
			String nodeName = node.getNodeName();
			String nodeValue = node.getNodeValue();
			if (nodeValue == null) {
				nodeValue = "null";
			}
			String namespaceURI = node.getNamespaceURI();
			if (namespaceURI == null) {
				namespaceURI = "null";
			}
			String namespacePrefix = node.getPrefix();
			if (namespacePrefix == null) {
				namespacePrefix = "null";
			}
			String localName = node.getLocalName();
			if (localName == null) {
				localName = "null";
			}
			
			System.out.println("result #: " +  "\n"
					+ "\tNode name: " + nodeName + "\n"
					+ "\tNode value: " + nodeValue + "\n"
					+ "\tNamespace URI: " + namespaceURI + "\n"
					+ "\tNamespace prefix: " + namespacePrefix + "\n"
					+ "\tLocal name: " + localName);
		}

	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		  listeners.add(l);
		 }
		 public void removePropertyChangeListener(PropertyChangeListener l) {
		  listeners.remove(l);
		 }




		public Document getDocument() {
			return xmlDoc;
		}
		
		public void setDocument(Document xmlDoc) {
			this.xmlDoc = xmlDoc;
		}
		 
}

