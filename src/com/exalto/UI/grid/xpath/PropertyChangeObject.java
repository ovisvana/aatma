/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.exalto.UI.grid.xpath;

import com.exalto.UI.grid.ExaltoXmlNode;

/**
 *
 * @author omprakash.v
 */
public class PropertyChangeObject {

	ExaltoXmlNode foundNode;
    String xpathExpression;

        public ExaltoXmlNode getFoundNode() {
            return foundNode;
        }

        public void setFoundNode(ExaltoXmlNode foundNode) {
            this.foundNode = foundNode;
        }

        public String getXpathExpression() {
            return xpathExpression;
        }

        public void setXpathExpression(String xpathExpression) {
                this.xpathExpression = xpathExpression;
        }


	}
		 
