/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * XmlTreeTableModel.java
 *
 * Created on October 31, 2006, 5:44 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.exalto.UI.grid;
import javax.swing.event.TreeModelEvent;

import com.exalto.UI.grid.TreeTableModel;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.tree.TreeNode;
import org.w3c.dom.Document;

/**
 *
 * @author omprakash.v
 */
public interface XmlTreeModel extends TreeTableModel {

    public ArrayList getParentList();
    
    public Document getDocument();
    
    public TreeNode[] getPathToRoot(TreeNode en);
    
    public void setRowMapper(HashMap mapper);
    
    public void setParentList(ArrayList plist);
    
    public Object getRoot();
    
    public boolean getRootVisible();
    
    public HashMap getRowMapper();
    
    public Object getValueAt(Object node, int row, int col);
    
    public boolean isCellEditable(Object node, int row, int col);
    
    public ArrayList getColumnMappingList();
    
    public void fireTreeNodesInserted(TreeModelEvent tme);
   
    public void fireTreeNodesRemoved(TreeModelEvent tme);
    
    public void fireTreeStructureChanged(TreeModelEvent tme);
    
    
    public void setDocument(Document doc);
    
    public void update();
    
    public HashMap getDomToTreeMap();
    
}
