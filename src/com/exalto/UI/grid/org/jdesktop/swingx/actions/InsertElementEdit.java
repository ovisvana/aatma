/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.grid.org.jdesktop.swingx.actions;


import javax.swing.tree.TreeNode;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import org.w3c.dom.Node;

/**
 * @author Santhosh Kumar T
 */
public class InsertElementEdit extends AbstractUndoableEdit {
    private UndoableModel model;
    private TreeNode element;
    private TreeNode parent;
    Node refNode;
    int refIndex;
    
    public InsertElementEdit(UndoableModel model, TreeNode parent, TreeNode element, Node refNode, int refIndex){
        this.model = model;
        this.element = element;
        this.parent = parent;
        this.refNode = refNode;
        this.refIndex = refIndex;

    }
    
    public void undo() throws CannotUndoException {
        super.undo();

        model.deleteNode(element, false, true);
    }
    
    public void redo() throws CannotRedoException {
        super.redo();

        model.insertNodeUndoRedo("Element", parent, element, refNode, refIndex);

    }
}
