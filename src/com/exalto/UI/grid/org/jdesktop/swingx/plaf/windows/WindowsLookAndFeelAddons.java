/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * $Id: WindowsLookAndFeelAddons.java,v 1.3 2005/10/10 18:02:24 rbair Exp $
 *
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package com.exalto.UI.grid.org.jdesktop.swingx.plaf.windows;


import org.jdesktop.swingx.plaf.basic.BasicLookAndFeelAddons;
import com.exalto.UI.grid.org.jdesktop.swingx.plaf.Addons;

/**
 * Adds new pluggable UI following the Windows XP look and feel.
 */
public class WindowsLookAndFeelAddons extends BasicLookAndFeelAddons implements Addons {

  public static final String HOMESTEAD_VISUAL_STYLE = "HomeStead";
  
  public static final String SILVER_VISUAL_STYLE = "Metallic";

}
