/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * Created on 13.06.2006
 *
 */
package com.exalto.UI.grid.org.jdesktop.swingx.event;

import java.beans.PropertyChangeEvent;

import javax.swing.event.TableColumnModelListener;

/**
 * A TableColumnModelListener which is interested in propertyChanges of
 * contained TableColumns.
 * 
 * @author Jeanette Winzenburg
 */
public interface TableColumnModelExtListener extends TableColumnModelListener {

    /**
     * 
     * @param event a <code>PropertyChangeEvent</code> fired by a TableColumn
     *   contained in the TableColumnModel
     */
    void columnPropertyChange(PropertyChangeEvent event);
}
