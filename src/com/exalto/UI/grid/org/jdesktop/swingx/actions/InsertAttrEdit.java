/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * InsertAttrEdit.java
 *
 * Created on October 27, 2006, 2:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.exalto.UI.grid.org.jdesktop.swingx.actions;

import javax.swing.tree.TreeNode;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import org.w3c.dom.Node;

/**
 *
 * @author omprakash.v
 */
public class InsertAttrEdit extends AbstractUndoableEdit  {
    private UndoableModel model;
    private TreeNode parent;
    private TreeNode attrNode;
      
    
    /** Creates a new instance of InsertAttrEdit */
    public InsertAttrEdit(UndoableModel model, TreeNode parent, TreeNode attrNode){
        this.model = model;
        this.parent = parent;
        this.attrNode = attrNode;

    }
    
    public void undo() throws CannotUndoException {
        super.undo();
        model.deleteNodeFromJTable(parent, attrNode, true, true);
    }
    
    public void redo() throws CannotRedoException {
        super.redo();

        	model.insertNodeUndoRedo("Attr", parent, attrNode, null, 0);
        
    }

    
}
