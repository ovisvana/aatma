/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * $Id: JXTreeTable.java,v 1.60 2006/10/24 09:34:16 kleopatra Exp $
 *
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


package com.exalto.UI.grid.org.jdesktop.swingx;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.jdesktop.swingx.JXTree;

import com.exalto.UI.grid.ComponentAdapter;
import com.exalto.UI.grid.Highlighter;
import com.exalto.UI.grid.HighlighterPipeline;
import com.exalto.UI.grid.PatternHighlighter;
import com.exalto.UI.grid.SearchHighlighter;
import com.exalto.UI.grid.TreeTableModel;
import com.exalto.UI.grid.org.jdesktop.swingx.decorator.FilterPipeline;
import com.exalto.UI.grid.org.jdesktop.swingx.treetable.AbstractTreeTableModel;
import com.exalto.UI.grid.org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import com.exalto.UI.grid.org.jdesktop.swingx.treetable.TreeTableCellEditor;
import com.exalto.UI.grid.org.jdesktop.swingx.UIAction;
import com.exalto.UI.util.AbstractSearchable;
import com.exalto.UI.util.Matcher;
import com.exalto.UI.util.AbstractSearchable.SearchResult;
import java.awt.Color;


/**
 * <p><code>JXTreeTable</code> is a specialized {@link javax.swing.JTable table}
 * consisting of a single column in which to display hierarchical data, and any
 * number of other columns in which to display regular data. The interface for
 * the data model used by a <code>JXTreeTable</code> is
 * {@link org.jdesktop.swingx.treetable.TreeTableModel}. It extends the
 * {@link javax.swing.tree.TreeModel} interface to allow access to cell data by
 * column indices within each node of the tree hierarchy.</p>
 *
 * <p>The most straightforward way create and use a <code>JXTreeTable</code>, is to
 * first create a suitable data model for it, and pass that to a
 * <code>JXTreeTable</code> constructor, as shown below:
 * <pre>
 *  TreeTableModel  treeTableModel = new FileSystemModel(); // any TreeTableModel
 *  JXTreeTable      treeTable = new JXTreeTable(treeTableModel);
 *  JScrollPane     scrollpane = new JScrollPane(treeTable);
 * </pre>
 * See {@link javax.swing.JTable} for an explanation of why putting the treetable
 * inside a scroll pane is necessary.</p>
 *
 * <p>A single treetable model instance may be shared among more than one
 * <code>JXTreeTable</code> instances. To access the treetable model, always call
 * {@link #getTreeTableModel() getTreeTableModel} and
 * {@link #setTreeTableModel(org.jdesktop.swingx.treetable.TreeTableModel) setTreeTableModel}.
 * <code>JXTreeTable</code> wraps the supplied treetable model inside a private
 * adapter class to adapt it to a {@link javax.swing.table.TableModel}. Although
 * the model adapter is accessible through the {@link #getModel() getModel} method, you
 * should avoid accessing and manipulating it in any way. In particular, each
 * model adapter instance is tightly bound to a single table instance, and any
 * attempt to share it with another table (for example, by calling
 * {@link #setModel(javax.swing.table.TableModel) setModel})
 * will throw an <code>IllegalArgumentException</code>!
 *
 * @author Philip Milne
 * @author Scott Violet
 * @author Ramesh Gupta
 */
public class JXTreeTable extends JXTable {
    private static final Logger LOG = Logger.getLogger(JXTreeTable.class
            .getName());
    /**
     * Key for clientProperty to decide whether to apply hack around #168-jdnc.
     */
    public static final String DRAG_HACK_FLAG_KEY = "treeTable.dragHackFlag";
    /**
     * Renderer used to render cells within the
     *  {@link #isHierarchical(int) hierarchical} column.
     *  renderer extends JXTree and implements TableCellRenderer
     */
    private TreeTableCellRenderer renderer;

    protected TreeTableHacker treeTableHacker;
    protected boolean consumedOnPress;
	boolean showingFind = false;
    boolean isEditing = false;
    boolean mightExpTrig;
    
    //OV added for undo/redo 031206
    boolean doingUndo = false;

    static int ctr = 0;

    /**
     * Constructs a JXTreeTable using a
     * {@link org.jdesktop.swingx.treetable.DefaultTreeTableModel}.
     */
    public JXTreeTable()  throws Exception {
        this(new DefaultTreeTableModel());
    }
    

    /**
     * Constructs a JXTreeTable using the specified
     * {@link org.jdesktop.swingx.treetable.TreeTableModel}.
     *
     * @param treeModel model for the JXTreeTable
     */
    public JXTreeTable(TreeTableModel treeModel)  throws Exception {
        // Implementation note:
        // Make sure that the SAME instance of treeModel is passed to the
        // constructor for TreeTableCellRenderer as is passed in the first
        // argument to the following chained constructor for this JXTreeTable:
        this(treeModel, new JXTreeTable.TreeTableCellRenderer(treeModel));
    }

    /**
     * Constructs a <code>JXTreeTable</code> using the specified
     * {@link org.jdesktop.swingx.treetable.TreeTableModel} and
     * {@link org.jdesktop.swingx.JXTreeTable.TreeTableCellRenderer}. The renderer
     * must have been constructed using the same instance of
     * {@link org.jdesktop.swingx.treetable.TreeTableModel} as passed to this
     * constructor.
     *
     * @param treeModel model for the JXTreeTable
     * @param renderer cell renderer for the tree portion of this JXTreeTable instance.
     * @throws IllegalArgumentException if an attempt is made to instantiate
     * JXTreeTable and TreeTableCellRenderer with different instances of TreeTableModel.
     */
    private JXTreeTable(TreeTableModel treeModel, TreeTableCellRenderer renderer) throws Exception {
        // To avoid unnecessary object creation, such as the construction of a
        // DefaultTableModel, it is better to invoke super(TreeTableModelAdapter)
        // directly, instead of first invoking super() followed by a call to
        // setTreeTableModel(TreeTableModel).

        // Adapt tree model to table model before invoking super()
        super(new TreeTableModelAdapter(treeModel, renderer));


       System.out.println(" *********** in jxt 2 arg ctor ***************** ");


        // Enforce referential integrity; bail on fail
        if (treeModel != renderer.getModel()) { // do not use assert here!
            throw new IllegalArgumentException("Mismatched TreeTableModel");
        }

        // renderer-related initialization
       // init(renderer); // private method
       // initActions();
        // disable sorting
        super.setSortable(false);
        
        // no grid
 //       setDefaultMargins(false, false);

//        // No grid.
//        setShowGrid(false); // superclass default is "true"
//
//        // Default intercell spacing
//        setIntercellSpacing(spacing); // for both row margin and column margin

    }

    /**
     * Initializes this JXTreeTable and permanently binds the specified renderer
     * to it.
     *
     * @param renderer private tree/renderer permanently and exclusively bound
     * to this JXTreeTable.
     */

    protected void init(TreeTableCellRenderer renderer) {

       System.out.println(" *********** in jxt  init(TreeTableCellRenderer renderer)***************** ");


        this.renderer = renderer;
        // Force the JTable and JTree to share their row selection models.
        ListToTreeSelectionModelWrapper selectionWrapper =
            new ListToTreeSelectionModelWrapper();

        // JW: when would that happen?
        if (renderer != null) {
            renderer.bind(this); // IMPORTANT: link back!
            renderer.setSelectionModel(selectionWrapper);
        }
        // adjust the tree's rowHeight to this.rowHeight
        adjustTreeRowHeight(getRowHeight());

        setSelectionModel(selectionWrapper.getListSelectionModel());
        // install the renderer as default for hierarchicalColumn
        setDefaultRenderer(AbstractTreeTableModel.hierarchicalColumnClass,
            renderer);
        // Install the default editor.
         setDefaultEditor(AbstractTreeTableModel.hierarchicalColumnClass,
           new TreeTableCellEditor(renderer));
        
        // propagate the lineStyle property to the renderer
        PropertyChangeListener l = new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent evt) {
                JXTreeTable.this.renderer.putClientProperty(evt.getPropertyName(), evt.getNewValue());
                
            }
            
        };
        addPropertyChangeListener("JTree.lineStyle", l);
        
    }


    protected void initActions() {
        // Register the actions that this class can handle.
        ActionMap map = getActionMap();
        map.put("expand-all", new Actions("expand-all"));
        map.put("collapse-all", new Actions("collapse-all"));
      
        System.out.println(" treetable actionmap = " + map);
    
      /*  
        map.put("print", new Actions("print"));
        map.put("find", new Actions("find"));
   
        KeyStroke findStroke = KeyStroke.getKeyStroke("control F");
      //  getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(findStroke, "find");
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(findStroke, "find");
     
     
     
   	    ActionListener actionListener = new ActionListener() {
          public void actionPerformed(ActionEvent actionEvent) {
            try {
            	
            	System.out.println(" calling treetable find");
            	    showingFind = true; 
  					find();
            } catch (Exception badLocationException) {
              System.err.println("Oops");
            }
          }
        };
	
		*/
	

	//   addMouseListener(treeTableAdapter);
    }

    /**
     * A small class which dispatches actions.
     * TODO: Is there a way that we can make this static?
     */
    public class Actions extends UIAction {
        public Actions(String name) {
            super(name);
        }

        public void actionPerformed(ActionEvent evt) {
            if ("expand-all".equals(getName())) {
  //      expandAll();
            }
            else if ("collapse-all".equals(getName())) {
    //            collapseAll();
            }
            else if ("find".equals(getName())) {
		        System.out.println(" calling jxt find ");

                showingFind = true;
                find();
            }
            else if ("print".equals(getName())) {
               // print();
            }
            else if ("undo".equals(getName())) {
               // print();
                System.out.println(" undo....");
                undo();
                getTreeTableHacker().completeEditing();
            }
            else if ("redo".equals(getName())) {
               // print();
                 redo();
            	getTreeTableHacker().completeEditing();
                System.out.println(" redo....");
            }
            else if ("delete".equals(getName())) {
                // print();
            	delete();
             }

            
        }
    }

    /** 
     * overridden to do nothing. 
     * 
     * TreeTable is not sortable by default, because 
     * Sorters/Filters currently don't work properly.
     * 
     */
    
    public void setSortable(boolean sortable) {
        // no-op
    }

    
    /** 
     * overridden to do nothing. 
     * 
     * TreeTable is not sortable by default, because 
     * Sorters/Filters currently don't work properly.
     * 
     */
    
    public void setFilters(FilterPipeline pipeline) {
    }

    /**
     * Overriden to invoke repaint for the particular location if
     * the column contains the tree. This is done as the tree editor does
     * not fill the bounds of the cell, we need the renderer to paint
     * the tree in the background, and then draw the editor over it.
     * You should not need to call this method directly. <p>
     * 
     * Additionally, there is tricksery involved to expand/collapse
     * the nodes.
     *
     * {@inheritDoc}
     */
    
    public boolean editCellAt(int row, int column, EventObject e) {
    	
    //	System.out.println(" in jxt ECA mightExpTrig = " + mightExpTrig);
            	
        getTreeTableHacker().hitHandleDetectionFromEditCell(column, e);    // RG: Fix Issue 49!
        boolean canEdit = super.editCellAt(row, column, e);

   // 	System.out.println(" in jxt ECA canEdit =" + canEdit);
        
        if (canEdit && isHierarchical(column) && !showingFind && !mightExpTrig) {
        	
   	   // 	System.out.println(" in jxt ECA calling repaint ");        	
            isEditing = true;	
            repaint(getCellRect(row, column, false));
        }
        
       
        return canEdit;
    }

    /**
     * Overridden to enable hit handle detection a mouseEvent which triggered
     * a expand/collapse. 
     */

    protected void processMouseEvent(MouseEvent e) {

           // System.out.println(" in PME ");

        // BasicTableUI selects on released if the pressed had been 
        // consumed. So we try to fish for the accompanying released
        // here and consume it as wll.
         /*   
        if ((e.getID() == MouseEvent.MOUSE_RELEASED) && consumedOnPress) {
            System.out.println(" mouse event consuming");
            consumedOnPress = false;
            e.consume();
            return;
        }
        */    
        if (getTreeTableHacker().hitHandleDetectionFromProcessMouse(e)) {
            // Issue #332-swing: hacking around selection loss.
            // prevent the
            // _table_ selection by consuming the mouseEvent
            // if it resulted in a expand/collapse

			//System.out.println(" isHHDFPM consuming e clickcount =... " + e.getClickCount());
            
            consumedOnPress = true;
            e.consume();
            getTreeTableHacker().completeEditing();
            return;
        }
        consumedOnPress = false;
        
//	System.out.println(" HHDFPM not true. calling super ");

        super.processMouseEvent(e);
    }
    

    protected TreeTableHacker getTreeTableHacker() {
        if (treeTableHacker == null) {
            treeTableHacker = createTreeTableHacker();
        }
        return treeTableHacker;
    }
    
    protected TreeTableHacker createTreeTableHacker() {
  //      return new TreeTableHacker();
        return new TreeTableHackerExt();
    }

    /**
     * Temporary class to have all the hacking at one place. Naturally, it will
     * change a lot. The base class has the "stable" behaviour as of around
     * jun2006 (before starting the fix for 332-swingx). <p>
     * 
     * specifically:
     * 
     * <ol>
     * <li> hitHandleDetection triggeredn in editCellAt
     * </ol>
     * 
     */
    public class TreeTableHacker {

        protected boolean expansionChangedFlag;

        /**
         * Decision whether the handle hit detection
         *   should be done in processMouseEvent or editCellAt.
         * Here: returns false.
         * 
         * @return true for handle hit detection in processMouse, false
         *   for editCellAt.
         */
        protected boolean isHitDetectionFromProcessMouse() {
            return false;
        }

        /**
        * Entry point for hit handle detection called from editCellAt, 
        * does nothing if isHitDetectionFromProcessMouse is true;
        * 
        * @see #editCellAt(int, int, EventObject)
        * @see #isHitDetectionFromProcessMouse()
        */
        public void hitHandleDetectionFromEditCell(int column, EventObject e) {
            if (!isHitDetectionFromProcessMouse()) {
                expandOrCollapseNode(column, e);
            }
        }

        /**
         * Entry point for hit handle detection called from processMouse.
         * Does nothing if isHitDetectionFromProcessMouse is false. 
         * 
         * @return true if the mouseEvent triggered an expand/collapse in
         *   the renderer, false otherwise. 
         *   
         * @see #processMouseEvent(MouseEvent)
         * @see #isHitDetectionFromProcessMouse()
         */
        public boolean hitHandleDetectionFromProcessMouse(MouseEvent e) {
            if (!isHitDetectionFromProcessMouse())
                return false;
            
            int col = columnAtPoint(e.getPoint());
  
  		//	System.out.println(" in HHDFPM ");
            boolean hit = ((col >= 0) && expandOrCollapseNode(columnAtPoint(e
                    .getPoint()), e));

  	
            hit = hit|doingUndo;   

          //  System.out.println(" in HHDFPM ret = " + hit);
    
            return hit;
        }

        /**
         * complete editing if collapsed/expanded.
         * Here: any editing is always cancelled.
         * This is a rude fix to #120-jdnc: data corruption on
         * collapse/expand if editing. This is called from 
         * the renderer after expansion related state has changed.
         * PENDING JW: should it take the old editing path as parameter?
         * Might be possible to be a bit more polite, and internally
         * reset the editing coordinates? On the other hand: the rudeness
         * is the table's usual behaviour - which is often removing the editor
         * as first reaction to incoming events.
         *
         */
        protected void completeEditing() {
            if (isEditing()) {
                getCellEditor().cancelCellEditing();
             }
        }

        /**
         * Tricksery to make the tree expand/collapse.
         * <p>
         * 
         * This might be - indirectly - called from one of two places:
         * <ol>
         * <li> editCellAt: original, stable but buggy (#332, #222) the table's
         * own selection had been changed due to the click before even entering
         * into editCellAt so all tree selection state is lost.
         * 
         * <li> processMouseEvent: the idea is to catch the mouseEvent, check
         * if it triggered an expanded/collapsed, consume and return if so or 
         * pass to super if not.
         * </ol>
         * 
         * <p>
         * widened access for testing ...
         * 
         * 
         * @param column the column index under the event, if any.
         * @param e the event which might trigger a expand/collapse.
         * 
         * @return this methods evaluation as to whether the event triggered a
         *         expand/collaps
         */
        protected boolean expandOrCollapseNode(int column, EventObject e) {
            if (!isHierarchical(column))
                return false;
            mightExpTrig = mightBeExpansionTrigger(e);
    
            //  System.out.println(" mightBeExpansionTrigger = " + mightExpTrig);

              if (!mightExpTrig)
                return false;
            boolean changedExpansion = false;
            MouseEvent me = (MouseEvent) e;
            if (hackAroundDragEnabled(me)) {
                
      //		System.out.println(" in EOCN HADE ");

                /*
                 * Hack around #168-jdnc: dirty little hack mentioned in the
                 * forum discussion about the issue: fake a mousePressed if drag
                 * enabled. The usability is slightly impaired because the
                 * expand/collapse is effectively triggered on released only
                 * (drag system intercepts and consumes all other).
                 */
                me = new MouseEvent((Component) me.getSource(),
                        MouseEvent.MOUSE_PRESSED, me.getWhen(), me
                                .getModifiers(), me.getX(), me.getY(), me
                                .getClickCount(), me.isPopupTrigger());

            }
            // If the modifiers are not 0 (or the left mouse button),
            // tree may try and toggle the selection, and table
            // will then try and toggle, resulting in the
            // selection remaining the same. To avoid this, we
            // only dispatch when the modifiers are 0 (or the left mouse
            // button).
            
        //	System.out.println(" in EOCN II ");

            if (me.getModifiers() == 0
                    || me.getModifiers() == InputEvent.BUTTON1_MASK) {
                MouseEvent pressed = new MouseEvent(renderer, me.getID(), me
                        .getWhen(), me.getModifiers(), me.getX()
                        - getCellRect(0, column, false).x, me.getY(), me
                        .getClickCount(), me.isPopupTrigger());
                renderer.dispatchEvent(pressed);
                // For Mac OS X, we need to dispatch a MOUSE_RELEASED as well
                MouseEvent released = new MouseEvent(renderer,
                        java.awt.event.MouseEvent.MOUSE_RELEASED, pressed
                                .getWhen(), pressed.getModifiers(), pressed
                                .getX(), pressed.getY(), pressed
                                .getClickCount(), pressed.isPopupTrigger());
                renderer.dispatchEvent(released);
                /*
                if (expansionChangedFlag) {
                    changedExpansion = true;
                }
                 */
            }
           /*     
            expansionChangedFlag = false;
            return changedExpansion;
            */
                return true;
        }

        protected boolean mightBeExpansionTrigger(EventObject e) {
            if (!(e instanceof MouseEvent)) return false;
            MouseEvent me = (MouseEvent) e;
            if (!SwingUtilities.isLeftMouseButton(me)) return false;
            
            Object ob = e.getSource();
            
           // System.out.println(" evt src = " + ob.getClass().getName());
           // System.out.println(" evt cc = " + me.getClickCount());

            if (renderer.isLocationInExpandControl(me.getX(), me.getY())) {
                    return true;
            }
            
            return false;
  //          return (me.getID() == MouseEvent.MOUSE_PRESSED && me.getClickCount() <= 1);
        }

        /**
         * called from the renderer's setExpandedPath after
         * all expansion-related updates happend.
         *
         */
        protected void expansionChanged() {
            expansionChangedFlag = true;
        }

        public boolean shouldRestoreSelectionAfterExpansionEvent() {
            return true;
        }


    }

    /**
     * 
     * Note: currently this class looks a bit funny (only overriding
     * the hit decision method). That's because the "experimental" code
     * as of the last round moved to stable. But I expect that there's more
     * to come, so I leave it here.
     * 
     * <ol>
     * <li> hit handle detection in processMouse
     * </ol>
     */
    public class TreeTableHackerExt extends TreeTableHacker {


        /**
         * Here: returns true.
         * @inheritDoc
         */
        
        protected boolean isHitDetectionFromProcessMouse() {
            return true;
        }
        
        public boolean shouldRestoreSelectionAfterExpansionEvent() {
            return true;
        }
        

    }
    /**
     * decides whether we want to apply the hack for #168-jdnc. here: returns
     * true if dragEnabled() and the improved drag handling is not activated (or
     * the system property is not accessible). The given mouseEvent is not
     * analysed.
     * 
     * PENDING: Mustang?
     * 
     * @param me the mouseEvent that triggered a editCellAt
     * @return true if the hack should be applied.
     */
    protected boolean hackAroundDragEnabled(MouseEvent me) {
        Boolean dragHackFlag = (Boolean) getClientProperty(DRAG_HACK_FLAG_KEY);
        if (dragHackFlag == null) {
            // access and store the system property as a client property once
            String priority = null;
            try {
                priority = System.getProperty("sun.swing.enableImprovedDragGesture");

            } catch (Exception ex) {
                // found some foul expression or failed to read the property
            }
            dragHackFlag = new Boolean((priority == null));
            putClientProperty(DRAG_HACK_FLAG_KEY, dragHackFlag);
        }
        return getDragEnabled() && dragHackFlag.booleanValue();
    }

    /**
     * Overridden to provide a workaround for BasicTableUI anomaly. Make sure
     * the UI never tries to resize the editor. The UI currently uses different
     * techniques to paint the renderers and editors. So, overriding setBounds()
     * is not the right thing to do for an editor. Returning -1 for the
     * editing row in this case, ensures the editor is never painted.
     *
     * {@inheritDoc}
     */
    
    public int getEditingRow() {
        return isHierarchical(editingColumn) ? -1 : editingRow;
    }

    /**
     * Returns the actual row that is editing as <code>getEditingRow</code>
     * will always return -1.
     */
    private int realEditingRow() {
        return editingRow;
    }

    /**
     * Sets the data model for this JXTreeTable to the specified
     * {@link org.jdesktop.swingx.treetable.TreeTableModel}. The same data model
     * may be shared by any number of JXTreeTable instances.
     *
     * @param treeModel data model for this JXTreeTable
     */
    public void setTreeTableModel(TreeTableModel treeModel) {
        TreeTableModel old = getTreeTableModel();
        renderer.setModel(treeModel);
        ((TreeTableModelAdapter)getModel()).setTreeTableModel(treeModel);
        // Enforce referential integrity; bail on fail
        // JW: when would that happen? we just set it... 
        if (treeModel != renderer.getModel()) { // do not use assert here!
            throw new IllegalArgumentException("Mismatched TreeTableModel");
        }
        firePropertyChange("treeTableModel", old, getTreeTableModel());
    }

    /**
     * Returns the underlying TreeTableModel for this JXTreeTable.
     *
     * @return the underlying TreeTableModel for this JXTreeTable
     */
    public TreeTableModel getTreeTableModel() {
        return ((TreeTableModelAdapter) getModel()).getTreeTableModel();
    }

    /**
     * <p>Overrides superclass version to make sure that the specified
     * {@link javax.swing.table.TableModel} is compatible with JXTreeTable before
     * invoking the inherited version.</p>
     *
     * <p>Because JXTreeTable internally adapts an
     * {@link org.jdesktop.swingx.treetable.TreeTableModel} to make it a compatible
     * TableModel, <b>this method should never be called directly</b>. Use
     * {@link #setTreeTableModel(org.jdesktop.swingx.treetable.TreeTableModel) setTreeTableModel} instead.</p>
     *
     * <p>While it is possible to obtain a reference to this adapted
     * version of the TableModel by calling {@link javax.swing.JTable#getModel()},
     * any attempt to call setModel() with that adapter will fail because
     * the adapter might have been bound to a different JXTreeTable instance. If
     * you want to extract the underlying TreeTableModel, which, by the way,
     * <em>can</em> be shared, use {@link #getTreeTableModel() getTreeTableModel}
     * instead</p>.
     *
     * @param tableModel must be a TreeTableModelAdapter
     * @throws IllegalArgumentException if the specified tableModel is not an
     * instance of TreeTableModelAdapter
     */
    
    public final void setModel(TableModel tableModel) { // note final keyword
        if (tableModel instanceof TreeTableModelAdapter) {
            if (((TreeTableModelAdapter) tableModel).getTreeTable() == null) {
                // Passing the above test ensures that this method is being
                // invoked either from JXTreeTable/JTable constructor or from
                // setTreeTableModel(TreeTableModel)
                super.setModel(tableModel); // invoke superclass version

                ((TreeTableModelAdapter) tableModel).bind(this); // permanently bound
                // Once a TreeTableModelAdapter is bound to any JXTreeTable instance,
                // invoking JXTreeTable.setModel() with that adapter will throw an
                // IllegalArgumentException, because we really want to make sure
                // that a TreeTableModelAdapter is NOT shared by another JXTreeTable.
            }
            else {
                throw new IllegalArgumentException("model already bound");
            }
        }
        else {
            throw new IllegalArgumentException("unsupported model type");
        }
    }


    
    /*
    public void tableChanged(TableModelEvent e) {
        if (isStructureChanged(e) || isUpdate(e)) {
            super.tableChanged(e);
        } else {
            resizeAndRepaint();
        }
    }
    */
    
    /**
     *  Overridden to return a do-nothing mapper.
     *  
     */
    /*
    public SelectionMapper getSelectionMapper() {
        // JW: don't want to change super assumption 
        // (mapper != null) - the selection mapping will change 
        // anyway in Mustang (using core functionality)
        return NO_OP_SELECTION_MANAGER ;
    }

    private static final SelectionMapper NO_OP_SELECTION_MANAGER = new SelectionMapper() {

        private ListSelectionModel viewSelectionModel = new DefaultListSelectionModel();

        public ListSelectionModel getViewSelectionModel() {
            return viewSelectionModel;
            }

        public void setViewSelectionModel(ListSelectionModel viewSelectionModel) {
            this.viewSelectionModel = viewSelectionModel;
            }

        public void setFilters(FilterPipeline pipeline) {
            // do nothing
            }

        public void insertIndexInterval(int start, int length, boolean before) {
            // do nothing
            }

        public void removeIndexInterval(int start, int end) {
            // do nothing
            }

        public void setEnabled(boolean enabled) {
            // do nothing
            }

        public boolean isEnabled() {
            return false;
            }
            
        public void clearModelSelection() {
            // do nothing
    }
    };
    */
    /**
     * Throws UnsupportedOperationException because variable height rows are
     * not supported.
     *
     * @param row ignored
     * @param rowHeight ignored
     * @throws UnsupportedOperationException because variable height rows are
     * not supported
     */
    
    public final void setRowHeight(int row, int rowHeight) {
        throw new UnsupportedOperationException("variable height rows not supported");
    }

    /**
     * Sets the row height for this JXTreeTable and forwards the 
     * row height to the renderering tree.
     * 
     * @param rowHeight height of a row.
     */
    
    public void setRowHeight(int rowHeight) {
        super.setRowHeight(rowHeight);
        adjustTreeRowHeight(getRowHeight()); 
    }

    /**
     * Forwards tableRowHeight to tree.
     * 
     * @param tableRowHeight height of a row.
     */
    protected void adjustTreeRowHeight(int tableRowHeight) {
        if (renderer != null && renderer.getRowHeight() != tableRowHeight) {
            renderer.setRowHeight(tableRowHeight);
        }
    }

    /**
     * Forwards treeRowHeight to table. This is for completeness only: the
     * rendering tree is under our total control, so we don't expect 
     * any external call to tree.setRowHeight.
     * 
     * @param treeRowHeight height of a row.
     */
    protected void adjustTableRowHeight(int treeRowHeight) {
        if (getRowHeight() != treeRowHeight) {
            adminSetRowHeight(treeRowHeight);
        }
    }


    /**
     * <p>Overridden to ensure that private renderer state is kept in sync with the
     * state of the component. Calls the inherited version after performing the
     * necessary synchronization. If you override this method, make sure you call
     * this version from your version of this method.</p>
     *
     * <p>This version maps the selection mode used by the renderer to match the
     * selection mode specified for the table. Specifically, the modes are mapped
     * as follows:
     * <pre>
     *  ListSelectionModel.SINGLE_INTERVAL_SELECTION: TreeSelectionModel.CONTIGUOUS_TREE_SELECTION;
     *  ListSelectionModel.MULTIPLE_INTERVAL_SELECTION: TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION;
     *  any other (default): TreeSelectionModel.SINGLE_TREE_SELECTION;
     * </pre>
     *
     * {@inheritDoc}
     *
     * @param mode any of the table selection modes
     */
    
    public void setSelectionMode(int mode) {
        if (renderer != null) {
            switch (mode) {
                case ListSelectionModel.SINGLE_INTERVAL_SELECTION: {
                    renderer.getSelectionModel().setSelectionMode(
                        TreeSelectionModel.CONTIGUOUS_TREE_SELECTION);
                    break;
                }
                case ListSelectionModel.MULTIPLE_INTERVAL_SELECTION: {
                    renderer.getSelectionModel().setSelectionMode(
                        TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
                    break;
                }
                default: {
                    renderer.getSelectionModel().setSelectionMode(
                        TreeSelectionModel.SINGLE_TREE_SELECTION);
                    break;
                }
            }
        }
        super.setSelectionMode(mode);
    }

    /**
     * Overrides superclass version to provide support for cell decorators.
     *
     * @param renderer the <code>TableCellRenderer</code> to prepare
     * @param row the row of the cell to render, where 0 is the first row
     * @param column the column of the cell to render, where 0 is the first column
     * @return the <code>Component</code> used as a stamp to render the specified cell
     */    
       public Component prepareRenderer(TableCellRenderer renderer, int row,
        int column) {
        
        Component component = super.prepareRenderer(renderer, row, column);
        // MUST ALWAYS ACCESS dataAdapter through accessor method!!!
        ComponentAdapter    adapter = getComponentAdapter();
        adapter.row = row;
        adapter.column = column;
        
        return applyRenderer(component, adapter); 
    }


    /**
     * Performs necessary housekeeping before the renderer is actually applied.
     *
     * @param component
     * @param adapter component data adapter
     * @throws NullPointerException if the specified component or adapter is null
     */
    protected Component applyRenderer(Component component,
        ComponentAdapter adapter) {
        if (component == null) {
            throw new IllegalArgumentException("null component");
        }
        if (adapter == null) {
            throw new IllegalArgumentException("null component data adapter");
        }

        if (isHierarchical(adapter.column)) {
            // After all decorators have been applied, make sure that relevant
            // attributes of the table cell renderer are applied to the
            // tree cell renderer before the hierarchical column is rendered!
            TreeCellRenderer tcr = renderer.getCellRenderer();
            if (tcr instanceof JXTree.DelegatingRenderer) {
                tcr = ((JXTree.DelegatingRenderer) tcr).getDelegateRenderer();
                
            }
            if (tcr instanceof DefaultTreeCellRenderer) {
                DefaultTreeCellRenderer dtcr = ((DefaultTreeCellRenderer) tcr);
                if (adapter.isSelected()) {
                    dtcr.setTextSelectionColor(component.getForeground());
                    dtcr.setBackgroundSelectionColor(component.getBackground());
               } else {
                    dtcr.setTextNonSelectionColor(component.getForeground());
                    dtcr.setBackgroundNonSelectionColor(component.getBackground());
                }
            } 
        }
        return component;
    }

    /**
     * Sets the specified TreeCellRenderer as the Tree cell renderer.
     *
     * @param cellRenderer to use for rendering tree cells.
     */
    public void setTreeCellRenderer(TreeCellRenderer cellRenderer) {
        if (renderer != null) {
            renderer.setCellRenderer(cellRenderer);
        }
    }

    public TreeCellRenderer getTreeCellRenderer() {
        return renderer.getCellRenderer();
    }

    
    
    public String getToolTipText(MouseEvent event) {
        int column = columnAtPoint(event.getPoint());
        if (isHierarchical(column)) {
            int row = rowAtPoint(event.getPoint());
            return renderer.getToolTipText(event, row, column);
        }
        return super.getToolTipText(event);
    }
    
    /**
     * Sets the specified icon as the icon to use for rendering collapsed nodes.
     *
     * @param icon to use for rendering collapsed nodes
     */

    public void setCollapsedIcon(Icon icon) {
        renderer.setCollapsedIcon(icon);
    }
    
    /**
     * Sets the specified icon as the icon to use for rendering expanded nodes.
     *
     * @param icon to use for rendering expanded nodes
     */
  
    public void setExpandedIcon(Icon icon) {
        renderer.setExpandedIcon(icon);
    }

    /**
     * Sets the specified icon as the icon to use for rendering open container nodes.
     *
     * @param icon to use for rendering open nodes
     */
   /*  
    public void setOpenIcon(Icon icon) {
        renderer.setOpenIcon(icon);
    }
*/
    /**
     * Sets the specified icon as the icon to use for rendering closed container nodes.
     *
     * @param icon to use for rendering closed nodes
     */
  /*
    public void setClosedIcon(Icon icon) {
        renderer.setClosedIcon(icon);
    }
*/
    /**
     * Sets the specified icon as the icon to use for rendering leaf nodes.
     *
     * @param icon to use for rendering leaf nodes
     */
  /*
    public void setLeafIcon(Icon icon) {
        renderer.setLeafIcon(icon);
    }
*/
    /**
     * Overridden to ensure that private renderer state is kept in sync with the
     * state of the component. Calls the inherited version after performing the
     * necessary synchronization. If you override this method, make sure you call
     * this version from your version of this method.
     */
    
    public void clearSelection() {
        if (renderer != null) {
            renderer.clearSelection();
        }
        super.clearSelection();
    }

    /**
     * Collapses all nodes in the treetable.
     */
    /* 
    public void collapseAll() {
        renderer.collapseAll();
    }
*/
    /**
     * Expands all nodes in the treetable.
     */
  /*
    public void expandAll() {
        renderer.expandAll();
    }
*/
    /**
     * Collapses the node at the specified path in the treetable.
     *
     * @param path path of the node to collapse
     */
    public void collapsePath(TreePath path) {
        renderer.collapsePath(path);
    }

    /**
     * Expands the the node at the specified path in the treetable.
     *
     * @param path path of the node to expand
     */
    public void expandPath(TreePath path) {
        renderer.expandPath(path);
    }

    /**
     * Makes sure all the path components in path are expanded (except
     * for the last path component) and scrolls so that the 
     * node identified by the path is displayed. Only works when this
     * <code>JTree</code> is contained in a <code>JScrollPane</code>.
     * 
     * (doc copied from JTree)
     * 
     * PENDING: JW - where exactly do we want to scroll? Here: the scroll
     * is in vertical direction only. Might need to show the tree column?
     * 
     * @param path  the <code>TreePath</code> identifying the node to
     *          bring into view
     */
    public void scrollPathToVisible(TreePath path) {
        if (path == null) return;
        renderer.makeVisible(path);
        int row = getRowForPath(path);
//        scrollRowToVisible(row);
    }

    
    /**
     * Collapses the row in the treetable. If the specified row index is
     * not valid, this method will have no effect.
     */
    public void collapseRow(int row) {
        renderer.collapseRow(row);
    }

    /**
     * Expands the specified row in the treetable. If the specified row index is
     * not valid, this method will have no effect.
     */
    public void expandRow(int row) {
        renderer.expandRow(row);
    }

    
    /**
     * Returns true if the value identified by path is currently viewable, which
     * means it is either the root or all of its parents are expanded. Otherwise,
     * this method returns false.
     *
     * @return true, if the value identified by path is currently viewable;
     * false, otherwise
     */
    public boolean isVisible(TreePath path) {
        return renderer.isVisible(path);
    }

    /**
     * Returns true if the node identified by path is currently expanded.
     * Otherwise, this method returns false.
     *
     * @param path path
     * @return true, if the value identified by path is currently expanded;
     * false, otherwise
     */
    public boolean isExpanded(TreePath path) {
        return renderer.isExpanded(path);
    }

    /**
     * Returns true if the node at the specified display row is currently expanded.
     * Otherwise, this method returns false.
     *
     * @param row row
     * @return true, if the node at the specified display row is currently expanded.
     * false, otherwise
     */
    public boolean isExpanded(int row) {
        return renderer.isExpanded(row);
    }

    /**
     * Returns true if the node identified by path is currently collapsed, 
     * this will return false if any of the values in path are currently not 
     * being displayed.   
     *
     * @param path path
     * @return true, if the value identified by path is currently collapsed;
     * false, otherwise
     */
    public boolean isCollapsed(TreePath path) {
        return renderer.isCollapsed(path);
    }

    /**
     * Returns true if the node at the specified display row is collapsed.
     *
     * @param row row
     * @return true, if the node at the specified display row is currently collapsed.
     * false, otherwise
     */
    public boolean isCollapsed(int row) {
        return renderer.isCollapsed(row);
    }

    
    /**
     * Returns an <code>Enumeration</code> of the descendants of the
     * path <code>parent</code> that
     * are currently expanded. If <code>parent</code> is not currently
     * expanded, this will return <code>null</code>.
     * If you expand/collapse nodes while
     * iterating over the returned <code>Enumeration</code>
     * this may not return all
     * the expanded paths, or may return paths that are no longer expanded.
     *
     * @param parent  the path which is to be examined
     * @return an <code>Enumeration</code> of the descendents of 
     *		<code>parent</code>, or <code>null</code> if
     *		<code>parent</code> is not currently expanded
     */
    
    public Enumeration getExpandedDescendants(TreePath parent) {
    	return renderer.getExpandedDescendants(parent);
    }

    
    /**
     * Returns the TreePath for a given x,y location.
     *
     * @param x x value
     * @param y y value
     *
     * @return the <code>TreePath</code> for the givern location.
     */
     public TreePath getPathForLocation(int x, int y) {
        int row = rowAtPoint(new Point(x,y));
        if (row == -1) {
          return null;  
        }
        return renderer.getPathForRow(row);
     }

    /**
     * Returns the TreePath for a given row.
     *
     * @param row
     *
     * @return the <code>TreePath</code> for the given row.
     */
     public TreePath getPathForRow(int row) {
        return renderer.getPathForRow(row);
     }

     /**
      * Returns the row for a given TreePath.
      *
      * @param path
      * @return the row for the given <code>TreePath</code>.
      */
     public int getRowForPath(TreePath path) {
       return renderer.getRowForPath(path);
     }

//------------------------------ exposed Tree properties

     /**
      * Determines whether or not the root node from the TreeModel is visible.
      *
      * @param visible true, if the root node is visible; false, otherwise
      */
     public void setRootVisible(boolean visible) {
         renderer.setRootVisible(visible);
         // JW: the revalidate forces the root to appear after a 
         // toggling a visible from an initially invisible root.
         // JTree fires a propertyChange on the ROOT_VISIBLE_PROPERTY
         // BasicTreeUI reacts by (ultimately) calling JTree.treeDidChange
         // which revalidate the tree part. 
         // Might consider to listen for the propertyChange (fired only if there
         // actually was a change) instead of revalidating unconditionally.
         revalidate();
         repaint();
     }

     /**
      * Returns true if the root node of the tree is displayed.
      *
      * @return true if the root node of the tree is displayed
      */
     public boolean isRootVisible() {
         return renderer.isRootVisible();
     }


    /**
     * Sets the value of the <code>scrollsOnExpand</code> property for the tree
     * part. This property specifies whether the expanded paths should be scrolled
     * into view. In a look and feel in which a tree might not need to scroll
     * when expanded, this property may be ignored.
     *
     * @param scroll true, if expanded paths should be scrolled into view;
     * false, otherwise
     */
    public void setScrollsOnExpand(boolean scroll) {
        renderer.setScrollsOnExpand(scroll);
    }

    /**
     * Returns the value of the <code>scrollsOnExpand</code> property.
     *
     * @return the value of the <code>scrollsOnExpand</code> property
     */
    public boolean getScrollsOnExpand() {
        return renderer.getScrollsOnExpand();
    }

    /**
     * Sets the value of the <code>showsRootHandles</code> property for the tree
     * part. This property specifies whether the node handles should be displayed.
     * If handles are not supported by a particular look and feel, this property
     * may be ignored.
     *
     * @param visible true, if root handles should be shown; false, otherwise
     */
    public void setShowsRootHandles(boolean visible) {
        renderer.setShowsRootHandles(visible);
        repaint();
    }

    /**
     * Returns the value of the <code>showsRootHandles</code> property.
     *
     * @return the value of the <code>showsRootHandles</code> property
     */
    public boolean getShowsRootHandles() {
        return renderer.getShowsRootHandles();
    }

    /**
     * Sets the value of the <code>expandsSelectedPaths</code> property for the tree
     * part. This property specifies whether the selected paths should be expanded.
     *
     * @param expand true, if selected paths should be expanded; false, otherwise
     */
    public void setExpandsSelectedPaths(boolean expand) {
        renderer.setExpandsSelectedPaths(expand);
    }

    /**
     * Returns the value of the <code>expandsSelectedPaths</code> property.
     *
     * @return the value of the <code>expandsSelectedPaths</code> property
     */
    public boolean getExpandsSelectedPaths() {
        return renderer.getExpandsSelectedPaths();
    }


    /**
     * Returns the number of mouse clicks needed to expand or close a node.
     *
     * @return number of mouse clicks before node is expanded
     */
    public int getToggleClickCount() {
        return renderer.getToggleClickCount();
    }

    /**
     * Sets the number of mouse clicks before a node will expand or close.
     * The default is two. 
     *
     * @param clickCount the number of clicks required to expand/collapse a node.
     */
    public void setToggleClickCount(int clickCount) {
        renderer.setToggleClickCount(clickCount);
    }

    /**
     * Returns true if the tree is configured for a large model.
     * The default value is false.
     * 
     * @return true if a large model is suggested
     * @see #setLargeModel
     */
    public boolean isLargeModel() {
        return renderer.isLargeModel();
    }

    /**
     * Specifies whether the UI should use a large model.
     * (Not all UIs will implement this.) <p>
     * 
     * <strong>NOTE</strong>: this method is exposed for completeness - 
     * currently it's not recommended 
     * to use a large model because there are some issues 
     * (not yet fully understood), namely
     * issue #25-swingx, and probably #270-swingx. 
     * 
     * @param newValue true to suggest a large model to the UI
     */
    public void setLargeModel(boolean newValue) {
        renderer.setLargeModel(newValue);
        // JW: random method calling ... doesn't help
//        renderer.treeDidChange();
//        revalidate();
//        repaint();
    }

//------------------------------ exposed tree listeners
    
    /**
     * Adds a listener for <code>TreeExpansion</code> events.
     * 
     * TODO (JW): redirect event source to this. 
     * 
     * @param tel a TreeExpansionListener that will be notified 
     * when a tree node is expanded or collapsed
     */
    public void addTreeExpansionListener(TreeExpansionListener tel) {
        renderer.addTreeExpansionListener(tel);
    }

    /**
     * Removes a listener for <code>TreeExpansion</code> events.
     * @param tel the <code>TreeExpansionListener</code> to remove
     */
    public void removeTreeExpansionListener(TreeExpansionListener tel) {
        renderer.removeTreeExpansionListener(tel);
    }

    /**
     * Adds a listener for <code>TreeSelection</code> events.
     * TODO (JW): redirect event source to this. 
     * 
     * @param tsl a TreeSelectionListener that will be notified 
     * when a tree node is selected or deselected
     */
    public void addTreeSelectionListener(TreeSelectionListener tsl) {
        renderer.addTreeSelectionListener(tsl);
    }

    /**
     * Removes a listener for <code>TreeSelection</code> events.
     * @param tsl the <code>TreeSelectionListener</code> to remove
     */
    public void removeTreeSelectionListener(TreeSelectionListener tsl) {
        renderer.removeTreeSelectionListener(tsl);
    }

    /**
     * Adds a listener for <code>TreeWillExpand</code> events.
     * TODO (JW): redirect event source to this. 
     * 
     * @param tel a TreeWillExpandListener that will be notified 
     * when a tree node will be expanded or collapsed 
     */
    public void addTreeWillExpandListener(TreeWillExpandListener tel) {
        renderer.addTreeWillExpandListener(tel);
    }

    /**
     * Removes a listener for <code>TreeWillExpand</code> events.
     * @param tel the <code>TreeWillExpandListener</code> to remove
     */
    public void removeTreeWillExpandListener(TreeWillExpandListener tel) {
        renderer.removeTreeWillExpandListener(tel);
     }
 
    
    /**
     * Returns the selection model for the tree portion of the this treetable.
     *
     * @return selection model for the tree portion of the this treetable
     */
    public TreeSelectionModel getTreeSelectionModel() {
        return renderer.getSelectionModel();    // RG: Fix JDNC issue 41
    }

    /**
     * Overriden to invoke supers implementation, and then,
     * if the receiver is editing a Tree column, the editors bounds is
     * reset. The reason we have to do this is because JTable doesn't
     * think the table is being edited, as <code>getEditingRow</code> returns
     * -1, and therefore doesn't automaticly resize the editor for us.
     */
    
    public void sizeColumnsToFit(int resizingColumn) {
        /** TODO: Review wrt doLayout() */
        super.sizeColumnsToFit(resizingColumn);
        // rg:changed
        if (getEditingColumn() != -1 && isHierarchical(editingColumn)) {
            Rectangle cellRect = getCellRect(realEditingRow(),
                getEditingColumn(), false);
            Component component = getEditorComponent();
            component.setBounds(cellRect);
            component.validate();
        }
    }

    /**
     * Overridden to message super and forward the method to the tree.
     * Since the tree is not actually in the component hieachy it will
     * never receive this unless we forward it in this manner.
     */
    
    public void updateUI() {
        super.updateUI();
        if (renderer != null) {
            renderer.updateUI();

            // Do this so that the editor is referencing the current renderer
            // from the tree. The renderer can potentially change each time
            // laf changes. 
            // JW: Hmm ... really? The renderer is fixed, set once
            // in creating treetable... 
            // commented to fix #213-jdnc (allow custom editors)
            setDefaultEditor(AbstractTreeTableModel.hierarchicalColumnClass,
                new TreeTableCellEditor(renderer));


            if (getBackground() == null || getBackground() instanceof UIResource) {
                setBackground(renderer.getBackground());
            }
        }
    }

    /**
     * Determines if the specified column contains hierarchical nodes.
     *
     * @param column zero-based index of the column
     * @return true if the class of objects in the specified column implement
     * the {@link javax.swing.tree.TreeNode} interface; false otherwise.
     */
    
    public boolean isHierarchical(int column) {
        return AbstractTreeTableModel.hierarchicalColumnClass.isAssignableFrom(
            getColumnClass(column));
    }

    /**
     * ListToTreeSelectionModelWrapper extends DefaultTreeSelectionModel
     * to listen for changes in the ListSelectionModel it maintains. Once
     * a change in the ListSelectionModel happens, the paths are updated
     * in the DefaultTreeSelectionModel.
     */
    class ListToTreeSelectionModelWrapper extends DefaultTreeSelectionModel {
        /** Set to true when we are updating the ListSelectionModel. */
        protected boolean updatingListSelectionModel;

        public ListToTreeSelectionModelWrapper() {
            super();
            getListSelectionModel().addListSelectionListener
                (createListSelectionListener());
        }

        /**
         * Returns the list selection model. ListToTreeSelectionModelWrapper
         * listens for changes to this model and updates the selected paths
         * accordingly.
         */
        ListSelectionModel getListSelectionModel() {
            return listSelectionModel;
        }

        /**
         * This is overridden to set <code>updatingListSelectionModel</code>
         * and message super. This is the only place DefaultTreeSelectionModel
         * alters the ListSelectionModel.
         */
        
        public void resetRowSelection() {
            if (!updatingListSelectionModel) {
                updatingListSelectionModel = true;
                try {
                    super.resetRowSelection();
                }
                finally {
                    updatingListSelectionModel = false;
                }
            }
            // Notice how we don't message super if
            // updatingListSelectionModel is true. If
            // updatingListSelectionModel is true, it implies the
            // ListSelectionModel has already been updated and the
            // paths are the only thing that needs to be updated.
        }

        /**
         * Creates and returns an instance of ListSelectionHandler.
         */
        protected ListSelectionListener createListSelectionListener() {
            return new ListSelectionHandler();
        }

        /**
         * If <code>updatingListSelectionModel</code> is false, this will
         * reset the selected paths from the selected rows in the list
         * selection model.
         */
        protected void updateSelectedPathsFromSelectedRows() {
            if (!updatingListSelectionModel) {
                updatingListSelectionModel = true;
                try {
                    if (listSelectionModel.isSelectionEmpty()) {
                        clearSelection();
                    } else {
                        // This is way expensive, ListSelectionModel needs an
                        // enumerator for iterating.
                        int min = listSelectionModel.getMinSelectionIndex();
                        int max = listSelectionModel.getMaxSelectionIndex();

                        List paths = new ArrayList();
                        for (int counter = min; counter <= max; counter++) {
                            if (listSelectionModel.isSelectedIndex(counter)) {
                                TreePath selPath = renderer.getPathForRow(
                                    counter);

                                if (selPath != null) {
                                    paths.add(selPath);
                                }
                            }
                        }
                        setSelectionPaths((TreePath[])paths.toArray(new TreePath[paths.size()]));
                        // need to force here: usually the leadRow is adjusted 
                        // in resetRowSelection which is disabled during this method
                        leadRow = leadIndex;
                    }
                }
                finally {
                    updatingListSelectionModel = false;
                }
            }
        }

        /**
         * Class responsible for calling updateSelectedPathsFromSelectedRows
         * when the selection of the list changse.
         */
        class ListSelectionHandler implements ListSelectionListener {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    updateSelectedPathsFromSelectedRows();
                }
            }
        }
    }

    /**
     * 
     */
    public static class TreeTableModelAdapter extends AbstractTableModel {
        private TreeModelListener treeModelListener;
        /**
         * Maintains a TreeTableModel and a JTree as purely implementation details.
         * Developers can plug in any type of custom TreeTableModel through a
         * JXTreeTable constructor or through setTreeTableModel().
         *
         * @param model Underlying data model for the JXTreeTable that will ultimately
         * be bound to this TreeTableModelAdapter
         * @param tree TreeTableCellRenderer instantiated with the same model as
         * specified by the model parameter of this constructor
         * @throws IllegalArgumentException if a null model argument is passed
         * @throws IllegalArgumentException if a null tree argument is passed
         */
        public TreeTableModelAdapter(TreeTableModel model, JTree tree) throws Exception {
   
   			if(model == null || tree == null)
   			    throw new Exception("Model or tree is null");
   			    	

            this.tree = tree; // need tree to implement getRowCount()
            setTreeTableModel(model);

            tree.addTreeExpansionListener(new TreeExpansionListener() {
                // Don't use fireTableRowsInserted() here; the selection model
                // would get updated twice.
                public void treeExpanded(TreeExpansionEvent event) {
                    updateAfterExpansionEvent(event);
                }

                public void treeCollapsed(TreeExpansionEvent event) {
                    updateAfterExpansionEvent(event);
                }
            });
        }

        /**
         * updates the table after having received an TreeExpansionEvent.<p>
         * 
         * @param event the TreeExpansionEvent which triggered the method call.
         */
        protected void updateAfterExpansionEvent(TreeExpansionEvent event) {
            // moved to let the renderer handle directly
//            treeTable.getTreeTableHacker().setExpansionChangedFlag();
            // JW: delayed fire leads to a certain sluggishness occasionally? 
            fireTableDataChanged();
        }

        /**
         * 
         * @param model must not be null!
         */
        public void setTreeTableModel(TreeTableModel model) {
            TreeTableModel old = getTreeTableModel();
            if (old != null) {
                old.removeTreeModelListener(getTreeModelListener());
            }
            this.model = model;
            // Install a TreeModelListener that can update the table when
            // tree changes. 
            model.addTreeModelListener(getTreeModelListener());
            fireTableStructureChanged();
        }

        /**
         * @return <code>TreeModelListener</code>
         */
        private TreeModelListener getTreeModelListener() {
            if (treeModelListener == null) {
                treeModelListener = new TreeModelListener() {
                    // We use delayedFireTableDataChanged as we can
                    // not be guaranteed the tree will have finished processing
                    // the event before us.
                    public void treeNodesChanged(TreeModelEvent e) {
                        delayedFireTableDataChanged(e, 0);
                    }

                    public void treeNodesInserted(TreeModelEvent e) {
                        delayedFireTableDataChanged(e, 1);
                    }

                    public void treeNodesRemoved(TreeModelEvent e) {
                        delayedFireTableDataChanged(e, 2);
                    }

                    public void treeStructureChanged(TreeModelEvent e) {
                        delayedFireTableDataChanged();
                    }
                };
            }
            return treeModelListener;
        }

        /**
         * Returns the real TreeTableModel that is wrapped by this TreeTableModelAdapter.
         *
         * @return the real TreeTableModel that is wrapped by this TreeTableModelAdapter
         */
        public TreeTableModel getTreeTableModel() {
            return model;
        }

        /**
         * Returns the JXTreeTable instance to which this TreeTableModelAdapter is
         * permanently and exclusively bound. For use by
         * {@link org.jdesktop.swingx.JXTreeTable#setModel(javax.swing.table.TableModel)}.
         *
         * @return JXTreeTable to which this TreeTableModelAdapter is permanently bound
         */
        protected JXTreeTable getTreeTable() {
            return treeTable;
        }

        /**
         * Immutably binds this TreeTableModelAdapter to the specified JXTreeTable.
         *
         * @param treeTable the JXTreeTable instance that this adapter is bound to.
         */
        protected final void bind(JXTreeTable treeTable) {
            // Suppress potentially subversive invocation!
            // Prevent clearing out the deck for possible hijack attempt later!
            if (treeTable == null) {
                throw new IllegalArgumentException("null treeTable");
            }

            if (this.treeTable == null) {
                this.treeTable = treeTable;
            }
            else {
                throw new IllegalArgumentException("adapter already bound");
            }
        }

        // Wrappers, implementing TableModel interface.
        // TableModelListener management provided by AbstractTableModel superclass.

        
        public Class getColumnClass(int column) {
            return model.getColumnClass(column);
        }

        public int getColumnCount() {
            return model.getColumnCount();
        }

        
        public String getColumnName(int column) {
            return model.getColumnName(column);
        }

        public int getRowCount() {
            return tree.getRowCount();
        }

        public Object getValueAt(int row, int column) {
            // Issue #270-swingx: guard against invisible row
            Object node = nodeForRow(row);
            return node != null ? model.getValueAt(node, column) : null;
        }

        
        public boolean isCellEditable(int row, int column) {
            // Issue #270-swingx: guard against invisible row
            Object node = nodeForRow(row);
            return node != null ? model.isCellEditable(node, column) : false;
        }

        
        public void setValueAt(Object value, int row, int column) {
            // Issue #270-swingx: guard against invisible row
            Object node = nodeForRow(row);
            if (node != null) {
                model.setValueAt(value, node, column);
            }
        }

        protected Object nodeForRow(int row) {
            // Issue #270-swingx: guard against invisible row
            TreePath path = tree.getPathForRow(row);
            return path != null ? path.getLastPathComponent() : null;
        }

        /**
         * Invokes fireTableDataChanged after all the pending events have been
         * processed. SwingUtilities.invokeLater is used to handle this.
         */
        private void delayedFireTableDataChanged() {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    fireTableDataChanged();
                }
            });
        }

        /**
         * Invokes fireTableDataChanged after all the pending events have been
         * processed. SwingUtilities.invokeLater is used to handle this.
         */
        private void delayedFireTableDataChanged(final TreeModelEvent tme, final int typeChange) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    int indices[] = tme.getChildIndices();
                    TreePath path = tme.getTreePath();
                    if (indices != null) { 
                        if (tree.isExpanded(path)) { // Dont bother to update if the parent 
                                                    // node is collapsed
                            int startingRow = tree.getRowForPath(path)+1;
                            int min = Integer.MAX_VALUE;
                            int max = Integer.MIN_VALUE;
                            for (int i=0;i<indices.length;i++) {
                                if (indices[i] < min) {
                                    min = indices[i];
                                }
                                if (indices[i] > max) {
                                    max = indices[i];
                                }
                            }
                            switch (typeChange) {
                                case 0 :
                                    fireTableRowsUpdated(startingRow + min, startingRow+max);
                                break;
                                case 1: 
                                    fireTableRowsInserted(startingRow + min, startingRow+max);
                                break;
                                case 2:
                                    fireTableRowsDeleted(startingRow + min, startingRow+max);
                                break;
                            }
                        } else { 
                        // not expanded - but change might effect appearance of parent
                        // Issue #82-swingx
                            int row = tree.getRowForPath(path);
                            // fix Issue #247-swingx: prevent accidental structureChanged
                            // for collapsed path 
                            // in this case row == -1, which == TableEvent.HEADER_ROW
                            if (row >= 0) fireTableRowsUpdated(row, row);
                        }
                    }
                    else {  // case where the event is fired to identify root.
                        fireTableDataChanged();
                    }
                }
            });
        }





        private TreeTableModel model; // immutable
        protected final JTree tree; // immutable
        protected JXTreeTable treeTable = null; // logically immutable
    }

    public static class TreeTableCellRenderer extends JTree implements
        TableCellRenderer {
        
           protected boolean overwriteIcons;
           int depthOffset;
           int totalChildIndent;
           int leftChildIndent;
           int rightChildIndent;
           int lastWidth;
           boolean leftToRight;
        // Force user to specify TreeTableModel instead of more general TreeModel
        public TreeTableCellRenderer(TreeTableModel model) {
            super(model);
            putClientProperty("JTree.lineStyle", "None");
    //        setRootVisible(false); 
        // superclass default is "true"
            setShowsRootHandles(true); // superclass default is "false"
                /** TODO: Support truncated text directly in DefaultTreeCellRenderer. */
        //    setOverwriteRendererIcons(true);
          

     //       setCellRenderer(new ClippedTreeCellRenderer());
        }

        /**
         * Hack around #297-swingx: tooltips shown at wrong row.
         * 
         * The problem is that - due to much tricksery when rendering
         * the tree - the given coordinates are rather useless. As a 
         * consequence, super maps to wrong coordinates. This takes
         * over completely.
         * 
         * PENDING: bidi?
         * 
         * @param event the mouseEvent in treetable coordinates
         * @param row the view row index
         * @param column the view column index
         * @return the tooltip as appropriate for the given row
         */
        private String getToolTipText(MouseEvent event, int row, int column) {
            if (row < 0) return null;
            TreeCellRenderer renderer = getCellRenderer();
            TreePath     path = getPathForRow(row);
            Object       lastPath = path.getLastPathComponent();
            Component    rComponent = renderer.getTreeCellRendererComponent
                (this, lastPath, isRowSelected(row),
                 isExpanded(row), getModel().isLeaf(lastPath), row,
                 true);

            if(rComponent instanceof JComponent) {
                Rectangle       pathBounds = getPathBounds(path);
                Rectangle cellRect = treeTable.getCellRect(row, column, false);
                // JW: what we are after
                // is the offset into the hierarchical column 
                // then intersect this with the pathbounds   
                Point mousePoint = event.getPoint();
                // translate to coordinates relative to cell
                mousePoint.translate(-cellRect.x, -cellRect.y);
                // translate horizontally to 
                mousePoint.translate(-pathBounds.x, 0);
                // show tooltip only if over renderer?
//                if (mousePoint.x < 0) return null;
//                p.translate(-pathBounds.x, -pathBounds.y);
                MouseEvent newEvent = new MouseEvent(rComponent, event.getID(),
                      event.getWhen(),
                      event.getModifiers(),
                      mousePoint.x, 
                      mousePoint.y,
//                    p.x, p.y, 
                      event.getClickCount(),
                      event.isPopupTrigger());
                
                return ((JComponent)rComponent).getToolTipText(newEvent);
            }

            return null;
        }

        /**
         * Immutably binds this TreeTableModelAdapter to the specified JXTreeTable.
         * For internal use by JXTreeTable only.
         *
         * @param treeTable the JXTreeTable instance that this renderer is bound to
         */
        public final void bind(JXTreeTable treeTable) {
            // Suppress potentially subversive invocation!
            // Prevent clearing out the deck for possible hijack attempt later!
            if (treeTable == null) {
                throw new IllegalArgumentException("null treeTable");
            }

            if (this.treeTable == null) {
                this.treeTable = treeTable;
            }
            else {
                throw new IllegalArgumentException("renderer already bound");
            }
        }

        
        
        protected void setExpandedState(TreePath path, boolean state) {
          //  System.out.println(" in  setExpandedState ");
            int count = getRowCount();
            super.setExpandedState(path, state);
            treeTable.getTreeTableHacker().expansionChanged();
            treeTable.getTreeTableHacker().completeEditing();
            
        }

        /**
         * updateUI is overridden to set the colors of the Tree's renderer
         * to match that of the table.
         */
        
        public void updateUI() {
            super.updateUI();
            // Make the tree's cell renderer use the table's cell selection
            // colors.
            // TODO JW: need to revisit...
            // a) the "real" of a JXTree is always wrapped into a DelegatingRenderer
            //  consequently the if-block never executes
            // b) even if it does it probably (?) should not 
            // unconditionally overwrite custom selection colors. 
            // Check for UIResources instead. 
            TreeCellRenderer tcr = getCellRenderer();
            if (tcr instanceof DefaultTreeCellRenderer) {
                DefaultTreeCellRenderer dtcr = ((DefaultTreeCellRenderer) tcr);
                // For 1.1 uncomment this, 1.2 has a bug that will cause an
                // exception to be thrown if the border selection color is null.
                dtcr.setBorderSelectionColor(null);
                dtcr.setTextSelectionColor(
                    UIManager.getColor("Table.selectionForeground"));
                dtcr.setBackgroundSelectionColor(
                    UIManager.getColor("Table.selectionBackground"));
            }
        }

        /**
         * Sets the row height of the tree, and forwards the row height to
         * the table.
         * 
         *
         */
        
        public void setRowHeight(int rowHeight) {
            // JW: can't ... updateUI invoked with rowHeight = 0
            // hmmm... looks fishy ...
//            if (rowHeight <= 0) throw 
//               new IllegalArgumentException("the rendering tree must have a fixed rowHeight > 0");
            super.setRowHeight(rowHeight);
            if (rowHeight > 0) {
                if (treeTable != null) {
                    treeTable.adjustTableRowHeight(rowHeight);
                }
            }
        }
        
          
      /**
     * sets the icon for the handle of an expanded node.
     * 
     * Note: this will only succeed if the current ui delegate is
     * a BasicTreeUI otherwise it will do nothing.
     * 
     * @param expanded
     */
    public void setExpandedIcon(Icon expanded) {
        if (getUI() instanceof BasicTreeUI) {
            ((BasicTreeUI) getUI()).setExpandedIcon(expanded);
        }
    }
    
    /**
     * sets the icon for the handel of a collapsed node.
     * 
     * Note: this will only succeed if the current ui delegate is
     * a BasicTreeUI otherwise it will do nothing.
     *  
     * @param collapsed
     */
    public void setCollapsedIcon(Icon collapsed) {
        if (getUI() instanceof BasicTreeUI) {
            ((BasicTreeUI) getUI()).setCollapsedIcon(collapsed);
        }
    }
    
    /**
     * set the icon for a leaf node.
     * 
     * Note: this will only succeed if current renderer is a 
     * DefaultTreeCellRenderer.
     * 
     * @param leafIcon
     */
   /*
    public void setLeafIcon(Icon leafIcon) {
        getDelegatingRenderer().setLeafIcon(leafIcon);
    }
  */      
 
      public boolean isOverwriteRendererIcons() {
            return overwriteIcons;
      }

    
    
  /**
     * Property to control whether per-tree icons should be
     * copied to the renderer on setCellRenderer.
     *
     * the default is false for backward compatibility.
     *
     * PENDING: should update the current renderer's icons when
     * setting to true?
     *
     * @param overwrite
     */
    public void setOverwriteRendererIcons(boolean overwrite) {
        
        
        if (overwriteIcons == overwrite) {
         //   System.out.println("overwrite true. returning... ");
            return;
        }

      //  System.out.println("overwrite different. setting... " + overwriteIcons);

        boolean old = overwriteIcons;
        this.overwriteIcons = overwrite;
    }
              /**
         * tries to set the renderers icons. Can succeed only if the
         * delegate is a DefaultTreeCellRenderer.
         * THINK: how to update? always override with this.icons, only
         * if renderer's icons are null, update this icons if they are not,
         * update all if only one is != null.... ??
         * 
         */
        public void updateIcons() {
        }
        
        
        private boolean isLocationInExpandControl(int mouseX, int mouseY) {
            
            JTree tree = (JTree) this;
            
            lastWidth = tree.getWidth();
            leftToRight = tree.getComponentOrientation().isLeftToRight();

            TreePath path = tree.getPathForLocation(mouseX, mouseY);

          //  System.out.println(" treepath = " + path);
            
            if(path == null)
                return true;
         /*      
            TreeModel tm = getModel();
         
            if(path != null && !treeModel.isLeaf(path.getLastPathComponent())){
                int                     boxWidth;
                Insets                  i = tree.getInsets();
 
                
                if(((BasicTreeUI)tree.getUI()).getExpandedIcon() != null)
                    boxWidth = ((BasicTreeUI)tree.getUI()).getExpandedIcon().getIconWidth();
                else
                    boxWidth = 8;

                int boxLeftX = (i != null) ? i.left : 0;

                updateDepthOffset();
                TreeUI tui = tree.getUI();
                if(tui instanceof BasicTreeUI) {
                    BasicTreeUI btui = (BasicTreeUI) tui;
                    leftChildIndent = btui.getLeftChildIndent();
                    rightChildIndent = btui.getRightChildIndent();
                    totalChildIndent = leftChildIndent + rightChildIndent;
                }
                    
                
                if (leftToRight) {
                   boxLeftX += (((path.getPathCount() + depthOffset - 2) *
			      totalChildIndent) + leftChildIndent) -
		              boxWidth / 2;
                }
                else {
                    boxLeftX += lastWidth - 1 -
		            ((path.getPathCount() - 2 + depthOffset) *
			     totalChildIndent) - leftChildIndent -
		            boxWidth / 2;
                }
	    
                int boxRightX = boxLeftX + boxWidth;

        	return mouseX >= boxLeftX && mouseX <= boxRightX;
	}
          *
          */
	return false;

        }
        
            /**
     * Updates how much each depth should be offset by.
     */
    protected void updateDepthOffset() {
	if(isRootVisible()) {
	    if(getShowsRootHandles())
		depthOffset = 1;
	    else
		depthOffset = 0;
	}
	else if(!getShowsRootHandles())
	    depthOffset = -1;
	else
	    depthOffset = 0;


    }

        
        /**
         * This is overridden to set the height to match that of the JTable.
         */
        
        public void setBounds(int x, int y, int w, int h) {
            if (treeTable != null) {
                y = 0;
                // It is not enough to set the height to treeTable.getHeight()
                h = treeTable.getRowCount() * this.getRowHeight();
            }
            super.setBounds(x, y, w, h);
        }

        /**
         * Sublcassed to translate the graphics such that the last visible row
         * will be drawn at 0,0.
         */
        
        public void paint(Graphics g) {
            Rectangle cellRect = treeTable.getCellRect(visibleRow, 0, false);
            g.translate(0, -cellRect.y);

            hierarchicalColumnWidth = getWidth();
            super.paint(g);

            // Draw the Table border if we have focus.
            if (highlightBorder != null) {
                // #170: border not drawn correctly
                // JW: position the border to be drawn in translated area
                // still not satifying in all cases...
                // RG: Now it satisfies (at least for the row margins)
                // Still need to make similar adjustments for column margins...
                highlightBorder.paintBorder(this, g, 0, cellRect.y,
                        getWidth(), cellRect.height);
            }
        }

        public Component getTableCellRendererComponent(JTable table,
            Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
            	
            if(table != treeTable)
                return null;
   
            
            if (isSelected) {
                setBackground(table.getSelectionBackground());
                setForeground(table.getSelectionForeground());
            }
            else {
                setBackground(table.getBackground());
                setForeground(table.getForeground());
            }

            highlightBorder = null;
            if (treeTable != null) {
                
               /* 
                if (treeTable.realEditingRow() == row &&
                    treeTable.getEditingColumn() == column) {
                
                System.out.println(" in if RER = " + treeTable.realEditingRow());
                    
                }
                else 
                */
                
                
                    if (hasFocus) {
                    
                     //   System.out.println(" in else HF = ");
                    highlightBorder = UIManager.getBorder(
                        "Table.focusCellHighlightBorder");
                }
             //   System.out.println(" in if RER = " + treeTable.realEditingRow());
                
                
            }

            
            visibleRow = row;

            return this;
        }
        
        private class ClippedTreeCellRenderer extends DefaultTreeCellRenderer {
            public void paint(Graphics g) {
                String fullText = super.getText();
                // getText() calls tree.convertValueToText();
                // tree.convertValueToText() should call treeModel.convertValueToText(), if possible

                String shortText = SwingUtilities.layoutCompoundLabel(
                    this, g.getFontMetrics(), fullText, getIcon(),
                    getVerticalAlignment(), getHorizontalAlignment(),
                    getVerticalTextPosition(), getHorizontalTextPosition(),
                    getItemRect(itemRect), iconRect, textRect,
                    getIconTextGap());

                /** TODO: setText is more heavyweight than we want in this
                 * situation. Make JLabel.text protected instead of private.
                 */

                setText(shortText); // temporarily truncate text
                super.paint(g);
                setText(fullText); // restore full text
            }

            private Rectangle getItemRect(Rectangle itemRect) {
                getBounds(itemRect);
                itemRect.width = hierarchicalColumnWidth - itemRect.x;
                return itemRect;
            }

            // Rectangles filled in by SwingUtilities.layoutCompoundLabel();
            private final Rectangle iconRect = new Rectangle();
            private final Rectangle textRect = new Rectangle();
            // Rectangle filled in by this.getItemRect();
            private final Rectangle itemRect = new Rectangle();
        }

        /** Border to draw around the tree, if this is non-null, it will
         * be painted. */
        protected Border highlightBorder = null;
        protected JXTreeTable treeTable = null;
        protected int visibleRow = 0;

        // A JXTreeTable may not have more than one hierarchical column
        private int hierarchicalColumnWidth = 0;
    }

    /**
     * Returns the adapter that knows how to access the component data model.
     * The component data adapter is used by filters, sorters, and highlighters.
     *
     * @return the adapter that knows how to access the component data model
     */
    
    public ComponentAdapter getComponentAdapter() {
        if (dataAdapter == null) {
            dataAdapter = new TreeTableDataAdapter(this); 
        }
        return dataAdapter;
    }


    private final static Dimension spacing = new Dimension(0, 2);

    public static class TreeTableDataAdapter extends JXTable.TableAdapter {
        private final JXTreeTable table;

        /**
         * Constructs a <code>TreeTableDataAdapter</code> for the specified
         * target component.
         *
         * @param component the target component
         */
        public TreeTableDataAdapter(JXTreeTable component) {
            super(component);
            table = component;
        }
        public JXTreeTable getTreeTable() {
            return table;
        }

        /**
         * {@inheritDoc}
         */
        
        public boolean isExpanded() {
            return table.isExpanded(row); 
        }

        /**
         * {@inheritDoc}
         */
        
        public boolean isLeaf() {
            // Issue #270-swingx: guard against invisible row
            TreePath path = table.getPathForRow(row);
            if (path != null) {
                return table.getTreeTableModel().isLeaf(path.getLastPathComponent());
            }
            // JW: this is the same as BasicTreeUI.isLeaf. 
            // Shouldn't happen anyway because must be called for visible rows only.
            return true; 
        }
        /**
         *
         * @return true if the cell identified by this adapter displays hierarchical
         *      nodes; false otherwise
         */
        
        public boolean isHierarchical() {
            return table.isHierarchical(column);
        }
    }
    
// basic search support    
   protected void find() {
       System.out.println(" in jxt find ");
   	
   } 
   	
   public void reset() {
   	
   }
   
       public void setEditing(boolean editing) {
        isEditing = editing;
    }
    	
       public boolean getEditing() {
          return isEditing;
    }
       
  public JTree getTree() {
    return renderer;
  }
  
  public void undo() {
  	
  }

  public void redo() {
  	
  }

  public void delete() {
	  	
  }


  
  public class TableSearchable extends AbstractSearchable {

      private SearchHighlighter searchHighlighter;

      public TableSearchable() {
          
          if(lastSearchResult == null)
              lastSearchResult = new SearchResult();
      }


      protected void findMatchAndUpdateState(String pattern, int startRow,
              boolean backwards) {
              	
            	
          System.out.println(" in TS FMAUS ");
              	
          SearchResult matchRow = null;
          if (backwards) {

	            System.out.println(" backwards ");

              // CHECK: off-one end still needed?
              // Probably not - the findXX don't have side-effects any longer
              // hmmm... still needed: even without side-effects we need to
              // guarantee calling the notfound update at the very end of the
              // loop.
              for (int r = startRow; r >= -1 && matchRow == null; r--) {
                  matchRow = findMatchBackwardsInRow(pattern, r);
                  updateState(matchRow);
              }
          } else {
          	
	            System.out.println(" forwards ");
         	
              for (int r = startRow; r <= getSize() && matchRow == null; r++) {
                  matchRow = findMatchForwardInRow(pattern, r);
                  updateState(matchRow);
              }
          }
          // KEEP - JW: Needed to update if loop wasn't entered!
          // the alternative is to go one off in the loop. Hmm - which is
          // preferable?
          // updateState(matchRow);
  	 

      }

      protected SearchResult findExtendedMatch(String pattern, int row) {
          return findMatchAt(pattern, row, lastSearchResult.foundColumn);
      }

      private SearchResult findMatchForwardInRow(String pattern, int row) {
      	
      	System.out.println(" in TS FMFIR ");
         
          int startColumn = (lastSearchResult.foundColumn < 0) ? 0 : lastSearchResult.foundColumn+1;
          if (isValidIndex(row)) {
          	
          	System.out.println(" in if isValidIndex row =  " + row);
         
          	
              for (int column = startColumn; column < getColumnCount(); column++) {
                  SearchResult result = findMatchAt(pattern, row, column);
                  if (result != null)
                      return result;
              }
          }
          return null;
      }

      private SearchResult findMatchBackwardsInRow(String pattern, int row) {
          int startColumn = (lastSearchResult.foundColumn < 0) ? getColumnCount() - 1
                  : lastSearchResult.foundColumn;
          if (isValidIndex(row)) {
              for (int column = startColumn; column >= 0; column--) {
                  SearchResult result = findMatchAt(pattern, row, column);
                  if (result != null)
                      return result;
              }
          }
          return null;
      }

      protected SearchResult findMatchAt(String pattern, int row, int column) {
     
         	System.out.println(" in TS FMA row =  " + row);
         	System.out.println(" in TS FMA column =  " + column);
	
      	
          Object value = getValueAt(row, column);
         
         	System.out.println(" value =  " + value);
	 
          if (value != null) {
              Matcher matcher = new Matcher(pattern, value.toString());
              if (matcher.find()) {

     	           	System.out.println(" found match ");
                  return createSearchResult(matcher, row, column);
              }
          }
          return null;
      }

      protected int adjustStartPosition(int startIndex, boolean backwards) {
   //       lastSearchResult.foundColumn = -1;
          return super.adjustStartPosition(startIndex, backwards);
      }

      protected int moveStartPosition(int startRow, boolean backwards) {
          if (backwards) {
              lastSearchResult.foundColumn--;
              if (lastSearchResult.foundColumn < 0) {
                  startRow--;
              }
          } else {
              lastSearchResult.foundColumn++;
              if (lastSearchResult.foundColumn >= getColumnCount()) {
                  lastSearchResult.foundColumn = -1;
                  startRow++;
              }
          }
          return startRow;
      }

      protected boolean isEqualStartIndex(final int startIndex) {
          return super.isEqualStartIndex(startIndex)
                  && isValidColumn(lastSearchResult.foundColumn);
      }

      private boolean isValidColumn(int column) {
          return column >= 0 && column < getColumnCount();
      }


      protected int getSize() {
          return getRowCount();
      }

		public void reset() {
  		System.out.println(" find dialog dismissed ");    	
      	getHighlighters().removeHighlighter(searchHighlighter);
		}
	
 

      protected void moveMatchMarker() {
      	
      	try {
			  	System.out.println(" in 3M ");
                       	
          int row = lastSearchResult.foundRow;
          int column = lastSearchResult.foundColumn;
          String pattern = lastSearchResult.pattern;
          
            	System.out.println(" in 3M row =" + row);
            	System.out.println(" in 3M column =" + column);
          
            	System.out.println(" in 3M markByHighlighter =" + markByHighlighter());
          
          if ((row < 0) || (column < 0)) {
              if (markByHighlighter()) {
                  getSearchHighlighter().setPattern(null);
              }
              return;
          }
          if (markByHighlighter()) {
          	
 			  	System.out.println(" in MBH true ");
	            Rectangle cellRect = getCellRect(row, column, true);
              if (cellRect != null) {
                  scrollRectToVisible(cellRect);
              }

              ensureInsertedSearchHighlighters();
              // TODO (JW) - cleanup SearchHighlighter state management
              getSearchHighlighter().setPattern(pattern);
              int modelColumn = convertColumnIndexToModel(column);
              
             	System.out.println(" in 3M modelColumn= " + modelColumn);
	            
              getSearchHighlighter().setHighlightCell(row, modelColumn);
          } else { // use selection
              changeSelection(row, column, false, false);
              if (!getAutoscrolls()) {
                  // scrolling not handled by moving selection
                  Rectangle cellRect = getCellRect(row, column, true);
                  if (cellRect != null) {
                      scrollRectToVisible(cellRect);
                  }
              }
          }
          
          } catch(Exception e) {
          	e.printStackTrace();
          }
          
      }

      private boolean markByHighlighter() {
//           return Boolean.TRUE.equals(getClientProperty(MATCH_HIGHLIGHTER));
			  return true;
      }


      private void ensureInsertedSearchHighlighters() {
      	try {
      	
          if (getHighlighters() == null) {
          	
			  	System.out.println(" in GH null ");
	
              setHighlighters(new HighlighterPipeline(
                      new Highlighter[] { getSearchHighlighter() }));
          } else if (!isInPipeline(getSearchHighlighter())) {
          	
  			  	System.out.println(" in SH !inpipeline ");
	
              getHighlighters().addHighlighter(getSearchHighlighter());
          }
          
          } catch(Exception e) {
          	e.printStackTrace();
          }
      }

      private boolean isInPipeline(PatternHighlighter searchHighlighter) {
          Highlighter[] inPipeline = getHighlighters().getHighlighters();
          if ((inPipeline.length > 0) && 
             (searchHighlighter.equals(inPipeline[inPipeline.length -1]))) {
              return true;
          }
          getHighlighters().removeHighlighter(searchHighlighter);
          return false;
      }


      private SearchHighlighter getSearchHighlighter() throws Exception {
          if (searchHighlighter == null) {
              searchHighlighter = createSearchHighlighter();
          }
          return searchHighlighter;
      }

      protected SearchHighlighter createSearchHighlighter() throws Exception {

			System.out.println(" class type = " + SearchHighlighter.class);
			
			
			System.out.println(" class type new = " + renderer.getCellRenderer().getClass().getName());

/*
			TreeCellRenderer treeCellRenderer = renderer.getCellRenderer();
			
			if(treeCellRenderer instanceof DelegatingRenderer) {
			
				DelegatingRenderer drender = (DelegatingRenderer) treeCellRenderer;
				
				TreeIconRenderer clistener = (TreeIconRenderer) drender.getDelegateRenderer();
*/			
	//			searchHighlighter = new SearchHighlighter((ChangeListener) renderer.getCellRenderer());
            
    			searchHighlighter = new SearchHighlighter(Color.GRAY, Color.BLUE);

				// OV added 250409
				addHighlighter(searchHighlighter);

//			}
			
      	return searchHighlighter;
      }

	}
/*
    public  Color getSelectionBackground() {
        return super.getSelectionBackground();
    }
*/
}
