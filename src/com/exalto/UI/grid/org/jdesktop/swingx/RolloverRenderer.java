/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * Created on 29.03.2006
 *
 */
package com.exalto.UI.grid.org.jdesktop.swingx;


/**
 * Interface to mark renderers as "live". <p>
 * 
 * PENDING: probably need methods to enabled/click taking a similar
 *   set of parameters as getXXComponent because the actual 
 *   outcome might depend on the given value. If so, we'll need
 *   to extend the XXRenderer interfaces.
 *   
 * @author Jeanette Winzenburg
 */
public interface RolloverRenderer {
    /**
     * 
     * @return true if rollover effects are on and clickable.
     */
    boolean isEnabled();
    
    /**
     * Same as AbstractButton.doClick(). It's up to client
     * code to prepare the renderer's component before calling
     * this method.
     *
     */
    void doClick();
}
