/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.grid.org.jdesktop.swingx.actions;


import com.exalto.UI.grid.ExaltoXmlNode;
import java.util.Vector;
import javax.swing.event.UndoableEditListener;
import javax.swing.tree.TreeNode;
import org.w3c.dom.Node;


/**
 * @author Santhosh Kumar T
 */
public interface UndoableModel{
    public void addUndoableEditListener(UndoableEditListener listener);
    public void removeUndoableEditListener(UndoableEditListener listener);
    public TreeNode deleteNode(TreeNode treeNode, boolean delete, boolean isUndoRedo);
    public void deleteNodeFromJTable(TreeNode parent, TreeNode treeNode, boolean delete, boolean isUndoRedo);
    public void insertNodeUndoRedo(String type, TreeNode parent, TreeNode child, Node refchild, int index);
    public Vector insertNodeInParent(TreeNode parent, TreeNode nodeElement, TreeNode oldNode, TreeNode [] childNodes, String nodeType, String newValue, Node refChild, int index); 
	public void insertElementUndoRedo(String nodeType, TreeNode parentNode, TreeNode child, TreeNode oldNode, TreeNode [] childNodes, Node refChild, int refIndex);
}
