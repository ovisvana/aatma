/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
package com.exalto.UI.mdi;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.text.html.*;
public class HelpWindow extends JFrame {
	private JTextPane jep;
	public HelpWindow(String s) {
		addWindowListener(
			new WindowAdapter() {
				public void windowClosing(WindowEvent we) {
					dispose();
				}
			}
		);
		jep = new JTextPane();
		jep.setStyledDocument(new HTMLDocument());
		try {
			jep.setPage(s);
		} catch (IOException ioe) {
		}
		getContentPane().add(new JScrollPane(jep));
		setSize(800, 600);
		show();
	}
}
