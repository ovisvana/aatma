/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * Clipboard.java
 *
 * Created on March 19, 2008, 10:37 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.exalto.UI;

import java.awt.event.ActionEvent;
import javax.swing.undo.UndoManager;

/**
 *
 * @author omprakash.v
 */
public interface TreeTableClipboard {
    
    public void cut(ActionEvent evt);
    public void copy(ActionEvent evt);
    public void paste(ActionEvent evt);
 //   public void undo();
 //   public void redo();

    public void undo(ActionEvent evt);
    public void redo(ActionEvent evt);
    public UndoManager getUndoManager();
    
}
