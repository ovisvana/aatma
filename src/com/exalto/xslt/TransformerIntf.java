/*******************************************************************************
 * Copyright (c) 2012 Omprakash Visvanathan.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Omprakash Visvanathan - initial API and implementation
 ******************************************************************************/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.exalto.xslt;

import java.io.InputStream;

/**
 *
 * @author omprakash.v
 */
public interface TransformerIntf {

    public void doTransform()  throws Exception ;

    public String getXsltOutput();
    
    public String getErrorMessages();

   public void doTransform(InputStream xmlFile, InputStream xslFile) throws Exception;

   public void setXslVersion(String version);
   
}
